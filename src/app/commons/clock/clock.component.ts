import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClockService, Reloj } from './clock.service';

@Component({
    selector: 'app-clock',
    templateUrl: './clock.component.html',
    styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {

    datos$: Observable<Date>;
    hora: number;
    minutos: string;
    dia: string;
    fecha: string;
    ampm: string;
    segundos: string;

    constructor(private clockService: ClockService) { }

    ngOnInit() {
        this.datos$ = this.clockService.getInfoReloj();
        this.datos$.subscribe(t => {
            this.hora = t.getHours() % 12;
            this.hora = this.hora ? this.hora : 12;
            this.minutos = (t.getMinutes() < 10) ? '0' + t.getMinutes() : t.getMinutes().toString();
            this.ampm = t.getHours() > 11 ? 'PM' : 'AM';
            this.fecha = t.toLocaleString('es', { day: '2-digit', month: 'long' }).replace('.', '').replace('-', ' ');
            this.dia = t.toLocaleString('es', { weekday: 'long' }).replace('.', '');
        });
    }

}
