import { NgModule } from '@angular/core';

import { SharedModule } from '../core/shared.module';

import { CommonsRoutingModule } from './commons.routing.module';

import { ValidateComponent } from './validate/validate.component';
import { RegisterComponent } from './register/register/register.component';
import { CompanyComponent } from './register/company/company.component';
import { BillingComponent } from './register/billing/billing.component';
import { ConfirmComponent } from './register/confirm/confirm.component';

import { CommonsComponent } from './commons.component';
import { LoaderComponent } from './loader/loader.component';
import { TranslateModule } from '@ngx-translate/core';
import { ClockComponent } from './clock/clock.component';
import { SendcodeComponent } from './forgot/password/sendcode/sendcode.component';
import { SecuritycodeComponent } from './forgot/password/securitycode/securitycode.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RestorePasswordComponent } from './forgot/password/restore-password/restore-password.component';
import { ConfirmPasswordComponent } from './forgot/password/confirm-password/confirm-password.component';
import { RestoreUserComponent } from './forgot/user/restore-user/restore-user.component';
import { ConfirmUserComponent } from './forgot/user/confirm-user/confirm-user.component';
import { CustomDirectivesModule } from '../directives/custom-directives.module';
import { VerifyComponent } from './register/verify/verify.component';
import { ActionComponent } from './action/action.component';

@NgModule({
  declarations: [
    RegisterComponent,
    VerifyComponent,
    ValidateComponent,
    CommonsComponent,
    CompanyComponent,
    BillingComponent,
    ConfirmComponent,
    LoaderComponent,
    SendcodeComponent,
    RestorePasswordComponent,
    ConfirmPasswordComponent,
    SecuritycodeComponent,
    RestoreUserComponent,
    ConfirmUserComponent,
    ClockComponent,
    ActionComponent
  ],
  exports: [
    LoaderComponent,
  ],
  imports: [
    CustomDirectivesModule,
    SharedModule,
    CommonsRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ]
})
export class CommonsModule {
}
