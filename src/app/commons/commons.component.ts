import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-commons',
  animations: [],
  templateUrl: './commons.component.html',
  styleUrls: ['./commons.component.scss']
})
export class CommonsComponent implements OnInit {  

  private landingPage = environment.landingPage.url;

  constructor(
    private translate: TranslateService,
    private router: Router) {
    translate.setDefaultLang('es');
    if (localStorage.getItem('portal-curr-lang')) {
      translate.use(localStorage.getItem('portal-curr-lang'));
    } else {
      translate.use('es');
    }
  }

  changeLanguage() {
    const currentLang = this.translate.currentLang;
    if (currentLang === 'es') {
      this.translate.use('de');
      localStorage.setItem('portal-curr-lang', 'de');
    } else {
      this.translate.use('es');
      localStorage.setItem('portal-curr-lang', 'es');
    }
    window.location.reload();
  }


  close() {
    window.location.href = this.landingPage;
    // this.router.navigateByUrl('[http://www.google.com]').then();
  }

  ngOnInit() {
  }

}