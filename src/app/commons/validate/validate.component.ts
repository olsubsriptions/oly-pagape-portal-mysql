import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SubscriptionsCommonsService } from '../../core/subscriptions-commons.service';
import { UserService } from 'src/app/adminservices/user.service';
import { AuthService } from 'src/app/core/auth.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationType, MaxLength, MinLength } from 'src/app/class/constants';
import { ForgotService } from '../forgot/forgot.service';
import { LoaderService } from 'src/app/adminservices/loader.service';

@Component({
    selector: 'app-validate',
    templateUrl: './validate.component.html',
    styleUrls: ['./validate.component.scss']
})
export class ValidateComponent implements OnInit {

    MaxLength: any = MaxLength;
    validateForm: FormGroup;
    submitted = false;
    disabled = false;
    errorMessage: string;
    passwordVisible: boolean;
    forgotPage: string[] = ['/commons/forgot/password'];
    registerPage: string[] = ['/commons/register'];
    restoreUserPage: string[] = ['/commons/restore/user'];
    // usernameRegex = /^(?=.{4,20}$)(?![._-])(?!.*[._-]{2})[a-zA-Z0-9._-]+(?<![._-])$/;
    // domainRegex = /^(?=.{4,20}$)(?![.])(?!.*[.]{2})[a-zA-Z0-9.]+(?<![.])$/;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private subcriptions: SubscriptionsCommonsService,
        private userService: UserService,
        private authService: AuthService,
        private modalService: NzModalService,
        private nzNotification: NzNotificationService,
        private forgotService: ForgotService,
        private loaderService: LoaderService,
        private translate: TranslateService) {
    }

    ngOnInit() {
        this.validateForm = this.fb.group({
            // username: [null, [Validators.required, Validators.pattern(this.usernameRegex)]],
            // domain: [null, [Validators.required, Validators.pattern(this.domainRegex)]],
            email: [null, [Validators.required, Validators.email, Validators.minLength(4)]],
            password: [null, [Validators.required, Validators.minLength(MinLength.PASSWORD)]]
        });
        // this.username.disable();
        // this.domain.disable();
        // this.validateForm.updateValueAndValidity();
    }

    // encrypt() {

    //     const test = this.cryptoService.aesEncrypt('Angel Quispe Arocutipa');
    //     console.log(test);

    //     const test2 = this.cryptoService.aesDecrypt('7f38cfd236220d85d1ddb61e9543cd0369d37cc3ba4009a9be47583bfe2e3f18');
    //     console.log(test2);
    // }

    submitForm(): void {
        this.disabled = true;
        this.submitted = true;
        this.errorMessage = null;
        if (this.validateForm.valid) {
            const splitedEmail = this.email.value.toLowerCase().split('@');
            const loginData = {
                username: splitedEmail[0],
                domain: splitedEmail[1],
                authType: AuthenticationType.EMAIL
            };
            this.subcriptions.getUserName(loginData)
                .subscribe((res) => {
                    if (res && res.status && res.user) {
                        this.userService.loginValidate(res.user).subscribe(
                            (instance: any) => {
                                if (instance.status) {
                                    this.authService.login(res.user, this.password.value)
                                        .then((credentials) => {
                                            this.router.navigate(['/admin']).then();
                                        })
                                        .catch((error) => {
                                            // eliminados de i18n
                                            // "auth/invalid-email": "El usuario no es válido o está corrupto.",
                                            // "auth/user-not-found": "Cuenta no encontrada.",
                                            // "auth/wrong-password": "La contraseña no es correcta.",
                                            this.loaderService.isLoading.next(false);
                                            this.errorMessage = 'ERROR.' + (error.code || 'UNKNOWN');
                                        }
                                        ).finally(() => {
                                            this.enableButton();
                                        });
                                } else {
                                    this.modalService.warning({
                                        nzTitle: this.translate.instant('MODAL.WARNING'),
                                        nzContent: this.translate.instant('MODAL.EMAIL_VERIFICATION_MESSAGE')
                                    });
                                    this.router.navigate(['/commons']).then();
                                }
                            }
                        );
                    } else if (res && res.message) {
                        this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
                    } else {
                        this.errorMessage = 'ERROR.UNKNOWN';
                    }
                }, (err) => {
                    this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
                }).add(() => {
                    this.enableButton();
                });
        } else {
            for (const field in this.validateForm.controls) {
                if (this.validateForm.controls[field]) {
                    this.validateForm.controls[field].markAsDirty();
                    this.validateForm.controls[field].updateValueAndValidity();
                }
            }
            this.enableButton();
        }
    }

    enableButton(): void {
        setTimeout(() => {
            this.disabled = false;
        }, 2000);
    }

    goRegister() {
        this.router.navigate(this.registerPage);
    }

    goForgotPassword() {
        // if (this.username.value && this.domain.value) {
        //     this.forgotService.forgotUser.user = this.username.value + '@' + this.domain.value;
        //     this.router.navigate(this.forgotPage);
        if (this.email.value) {
            this.forgotService.clearForgotUser();
            this.forgotService.forgotUser.user = this.email.value;
            this.router.navigate(this.forgotPage);
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.ERROR_USER')
            );
        }
    }

    // goRestoreUser() {
    //     if (this.username.value && this.domain.value) {
    //         this.forgotService.forgotUser.user = this.username.value + '@' + this.domain.value;
    //         this.router.navigate(this.restoreUserPage);
    //     } else {
    //         this.nzNotification.create(
    //             'warning',
    //             this.translate.instant('MODAL.WARNING'),
    //             this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.ERROR_USER')
    //         );
    //     }
    // }


    public loginGoogle() {
        this.authService.loginGoogle().then((credential) => {
            this.router.navigate(['/admin']).then();
        })
            .catch((error) => {
                this.errorMessage = 'ERROR.' + (error.code || 'UNKNOWN');
            }
            ).finally(() => {
                this.enableButton();
            });
    }

    get email() {
        return this.validateForm.get('email');
    }
    get password() {
        return this.validateForm.get('password');
    }
}

