import {Component, OnInit} from '@angular/core';
import {RegisterService} from '../register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  constructor(private router: Router, private stateService: RegisterService) {
  }

  ngOnInit() {
    if (!this.stateService.validateStates()) {
      this.back();
      return;
    }
    this.stateService.clearStates();
  }

  back() {
    this.router.navigate(['/commons']).then();
  }


}
