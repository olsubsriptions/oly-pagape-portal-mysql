import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzSizeLDSType } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { OrganizationService } from 'src/app/adminservices/organization.service';
import { MaxLength } from 'src/app/class/constants';

@Component({
    selector: 'app-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

    MaxLength: any = MaxLength;
    errorMessage: string;
    companyDataForm: FormGroup;
    disabled = false;
    inputSize: NzSizeLDSType = 'large';
    firstState: any = {};
    nextPage: string[] = ['/commons/billing'];
    lastPage: string[] = ['/commons/register'];

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private registerService: RegisterService,
        private organizationService: OrganizationService) {
        const companyData = this.registerService.state3 || {};
        this.companyDataForm = this.fb.group({
            company: [companyData.company, [Validators.required]],
            document: [companyData.document, [Validators.required]],
            state: [companyData.state, [Validators.required]],
            province: [companyData.province, [Validators.required]],
            city: [companyData.city, [Validators.required]],
            address1: [companyData.address1, [Validators.required]],
            address2: [companyData.address2, [Validators.required]],
        });
    }

    ngOnInit() {
        this.firstState = this.registerService.state1;
        if (!this.firstState) {
            this.back();
            return;
        }
    }

    submitForm(): void {
        this.disabled = true;
        if (this.companyDataForm.valid) {
            this.registerService.state3 = this.companyDataForm.getRawValue();
            this.router.navigate(this.nextPage).then();
        } else {
            for (const field in this.companyDataForm.controls) {
                if (this.companyDataForm.controls[field]) {
                    this.companyDataForm.controls[field].markAsDirty();
                    this.companyDataForm.controls[field].updateValueAndValidity();
                }
            }
        }
        this.enableButton();
    }

    enableButton(milliseconds: number = 2000): void {
        setTimeout(() => {
            this.disabled = false;
        }, milliseconds);
    }

    skip() {
        this.registerService.state3 = null;
        this.router.navigate(this.nextPage).then();
    }

    back() {
        this.router.navigate(this.lastPage).then();
    }

    validateDocument() {
        this.organizationService.validateOrganizationDocument(this.document.value).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.companyDataForm.patchValue({
                        company: instance.Data.organizationName
                    });
                    this.company.markAsDirty();
                    this.company.updateValueAndValidity();
                }

                this.companyDataForm.patchValue({
                    company: 'data de prueba'
                });
                this.company.markAsDirty();
                this.company.updateValueAndValidity();
            }
        );
    }

    resetValidation(event: Event) {
        this.companyDataForm.patchValue({
            company: null
        });
        this.company.markAsDirty();
        this.company.updateValueAndValidity();
    }

    get document() {
        return this.companyDataForm.get('document');
    }

    get company() {
        return this.companyDataForm.get('company');
    }
    get state() {
        return this.companyDataForm.get('state');
    }
    get province() {
        return this.companyDataForm.get('province');
    }
    get city() {
        return this.companyDataForm.get('city');
    }
    get address1() {
        return this.companyDataForm.get('address1');
    }
    get address2() {
        return this.companyDataForm.get('address2');
    }
}
