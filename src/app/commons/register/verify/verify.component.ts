import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService, NzSizeLDSType } from 'ng-zorro-antd';
import { MaxLength } from 'src/app/class/constants';
import { RegisterService } from '../register.service';

@Component({
    selector: 'app-verify',
    templateUrl: './verify.component.html',
    styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

    MaxLength: any = MaxLength;
    public userCode: string;
    public codeForm: FormGroup;
    public inputSize: NzSizeLDSType = 'large';
    private lastPage: string[] = ['/commons/register'];
    private nextPage: string[] = ['/commons/organization'];
    firstState: any = {};

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private registerService: RegisterService,
        private nzNotification: NzNotificationService,
        private translate: TranslateService
    ) { }

    ngOnInit() {

        this.createCodeForm();
        this.firstState = this.registerService.state1;

        if (!this.firstState) {
            this.router.navigate(this.lastPage);
        } else {
            this.userCode = this.firstState.validationPhoneCode;
        }
    }

    public createCodeForm() {

        this.codeForm = this.fb.group({
            code: [null, [Validators.required]]
        });

    }

    public goCompany() {
        if (this.codeValidate()) {
            this.router.navigate(this.nextPage);
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.WARNING_CODE')
            );
        }
    }

    private codeValidate(): boolean {
        return this.code.value === this.userCode ? true : false;
    }

    public back() {
        this.router.navigate(this.lastPage);
    }

    get code() {
        return this.codeForm.get('code');
    }
}
