import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NzSizeLDSType, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { RegisterService } from '../register.service';
import { SubscriptionsCommonsService } from 'src/app/core/subscriptions-commons.service';
import { NotificationService } from 'src/app/adminservices/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { Constants, AuthenticationType, MaxLength, MinLength } from 'src/app/class/constants';
import { NotificationTemplate } from 'src/app/class/enums';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    MaxLength: any = MaxLength;
    validateForm: FormGroup;
    disabled = false;
    validationCode: string;
    passwordVisible: boolean;
    inputSize: NzSizeLDSType = 'large';
    errorMessage: string;
    phoneNumberRegex = /^[1-9]\d{3,13}$/;
    countries: Country[] = [];
    nextPage: string[] = ['/commons/verify'];
    lastPage: string[] = [''];

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private registerService: RegisterService,
        private commonService: SubscriptionsCommonsService,
        private modalService: NzModalService,
        private notificationService: NotificationService,
        private nzNotification: NzNotificationService,
        private translate: TranslateService) {
        const state1 = this.registerService.state1 || {};

        this.validateForm = this.fb.group({
            // username: [state1.username, [Validators.required, Validators.pattern(this.usernameRegex)]],
            // domain: [state1.domain, [Validators.required, Validators.pattern(this.domainRegex)]],
            email: [state1.email, [Validators.required, Validators.email, Validators.minLength(4)]],
            country: [, [Validators.required]],
            phoneNumber: [state1.phoneNumber, [Validators.required, Validators.minLength(4)]],
            // validationPhoneCode: [null, [Validators.required, smsValidate(this.getPhoneCodeRegExp(this.validationCode))]],
            password: [, [Validators.required, Validators.minLength(MinLength.PASSWORD)]],
            checkPassword: [, [Validators.required, Validators.minLength(MinLength.PASSWORD), this.confirmPassword]],
        });
    }

    ngOnInit() {
        this.disabled = true;
        this.commonService.getCountryCodes()
            .subscribe(
                res => {
                    this.countries = res;
                },
                err => {
                    console.error(err);
                    this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
                },
                () => {
                    this.validateForm.controls.country.setValue(this.countries[0]);
                    this.enableButton(0);
                }
            );
    }

    confirmPassword = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { error: true, required: true };
        } else if (control.value !== this.validateForm.controls.password.value) {
            return { confirm: true, error: true };
        }
        return {};
    }

    submitForm(): void {
        this.disabled = true;
        this.errorMessage = null;

        if (this.validateForm.valid) {
            const loginData = this.validateForm.getRawValue();
            loginData.email = loginData.email.toLowerCase();
            loginData.authType = AuthenticationType.EMAIL;
            this.registerService.preValidateLoginData(loginData).subscribe(
                (instance: any) => {
                    if (instance && instance.status) {
                        this.registerService.state1 = loginData;
                        this.sendValidationPhoneCode();
                    } else if (instance && instance.message) {
                        this.errorMessage = 'ERROR.' + (instance.message || 'UNKNOWN');
                    } else {
                        this.errorMessage = 'ERROR.UNKNOWN';
                    }
                },
                (error: any) => {
                    console.error(error);
                    this.errorMessage = 'ERROR.' + (error.name || error.message || 'UNKNOWN');
                    this.enableButton();
                },
                () => {
                    this.enableButton();
                }
            );
        } else {
            for (const field in this.validateForm.controls) {
                if (this.validateForm.controls[field]) {
                    this.validateForm.controls[field].markAsDirty();
                    this.validateForm.controls[field].updateValueAndValidity();
                }
            }
            this.enableButton();
        }
    }

    enableButton(millisecons: number = 2000): void {
        setTimeout(() => {
            this.disabled = false;
        }, millisecons);
    }

    updateConfirmValidator(): void {
        Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
    }

    CancelRegistration() {
        this.registerService.clearStates();
    }

    sendValidationPhoneCode() {
        this.validationCode = String(this.generateValidationCode());
        if (this.phoneNumber.valid && this.country.valid) {
            console.log(this.validationCode);
            const request = {
                to: this.phoneNumber.value,
                subject: '',
                template: NotificationTemplate.PHONE_VALIDATION,
                messageData: [{
                    code: 'Codigo',
                    value: Constants.VERIFICATION_CODE_PREFIX + this.validationCode
                }]
            };

            this.notificationService.sendSms(request).subscribe(
                (instance: any) => {
                    if (instance.Result === 0) {
                        // this.updatePhoneCodeValidation();
                        this.registerService.state1.validationPhoneCode = this.validationCode;
                        this.nzNotification.create(
                            'success',
                            this.translate.instant('MODAL.INFO'),
                            this.translate.instant('MODAL.SMS'),
                        );
                        this.router.navigate(this.nextPage).then();
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
                // (error: any) => {
                //     this.modalService.warning({
                //         nzTitle: this.translate.instant('MODAL.WARNING'),
                //         nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                //     });
                // }
            );
        }
    }

    // resetValidation(event: Event) {
    //     this.validationCode = null;
    //     this.updatePhoneCodeValidation();
    // }

    // updatePhoneCodeValidation() {
    //     this.validationPhoneCode.setValidators(
    //         [Validators.required, smsValidate(this.getPhoneCodeRegExp(this.validationCode))]);
    //     this.validationPhoneCode.markAsDirty();
    //     this.validationPhoneCode.updateValueAndValidity();
    // }

    getPhoneCodeRegExp(code: string): RegExp {
        return code ? new RegExp('^' + code + '$') : null;
    }

    generateValidationCode() {
        return Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
    }

    get phoneNumber() {
        return this.validateForm.get('phoneNumber');
    }
    get email() {
        return this.validateForm.get('email');
    }
    get country() {
        return this.validateForm.get('country');
    }
    get validationPhoneCode() {
        return this.validateForm.get('validationPhoneCode');
    }
    get username() {
        return this.validateForm.get('username');
    }
    get domain() {
        return this.validateForm.get('domain');
    }
    get password() {
        return this.validateForm.get('password');
    }
    get checkPassword() {
        return this.validateForm.get('checkPassword');
    }

}

function smsValidate(code: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (code && control.value && control.value.match(code)) {
            return null;
        }
        return { smsValidate: true };
    };
}
