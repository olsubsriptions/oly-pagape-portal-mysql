import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzSizeLDSType, NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { NotificationService } from 'src/app/adminservices/notification.service';
import { environment } from 'src/environments/environment';
import { MaxLength } from 'src/app/class/constants';
import { NotificationTemplate, EmailSubject } from 'src/app/class/enums';
import { OrganizationService } from 'src/app/adminservices/organization.service';
import { AccountPpe } from 'src/app/class/accountPpe';
import { AccountService } from 'src/app/adminservices/account.service';
import { TranslateService } from '@ngx-translate/core';
import { AccountParameter } from 'src/app/class/accountParameter';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {

    MaxLength: any = MaxLength;
    billingDataForm: FormGroup;
    disabled = false;
    inputSize: NzSizeLDSType = 'large';
    firstState: any = {};
    errorMessage: string;
    copyData = true;
    private consoleUrl = environment.console.url;
    private accountPpe = new AccountPpe();
    public applicationSubscription: ApplicationSubscription;
    private accountParameters: Array<AccountParameter>;
    private applicationId = '2df6ba93-9de1-4737-8cfd-2e7c33cd3271';

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private registerService: RegisterService,
        private organizationService: OrganizationService,
        private accountService: AccountService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private notificationService: NotificationService) {
        const state4 = this.registerService.state4 || {};
        this.billingDataForm = this.fb.group({
            name: [state4.name, [Validators.required]],
            email: [state4.email, [Validators.email, Validators.required]],
            name2: [state4.name2, [Validators.required]],
            email2: [state4.email2, [Validators.email, Validators.required]],
        });
    }

    ngOnInit() {
        this.firstState = this.registerService.state1;
        if (!this.firstState) {
            this.back();
            return;
        }
    }

    submitForm(): void {
        this.disabled = true;
        this.errorMessage = null;
        if (this.copyData) {
            this.billingDataForm.controls.name2.setValue(this.billingDataForm.controls.name.value);
            this.billingDataForm.controls.email2.setValue(this.billingDataForm.controls.email.value);
        }
        if (this.billingDataForm.valid) {
            this.createUser();
        } else {
            for (const field in this.billingDataForm.controls) {
                if (this.billingDataForm.controls[field]) {
                    this.billingDataForm.controls[field].markAsDirty();
                    this.billingDataForm.controls[field].updateValueAndValidity();
                }
            }
            this.enableButton();
        }
    }

    createUser() {
        this.disabled = true;
        this.errorMessage = null;
        this.registerService.state4 = this.billingDataForm.getRawValue();
        this.registerService.state4.email = this.registerService.state4.email.toLowerCase();
        this.registerService.state4.email2 = this.registerService.state4.email2.toLowerCase();
        if (!this.registerService.validateStates()) {
            this.back();
            return;
        }
        this.registerService.createUser().subscribe(
            (res) => {
                if (res && res.status) {
                    this.registerSubscription(res.user, res.data);
                } else if (res && res.message) {
                    this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
                } else {
                    this.errorMessage = 'ERROR.UNKNOWN';
                }
            },
            (err) => {
                console.error(err);
                this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
                this.enableButton();
            },
            () => {
                this.enableButton();
            });
    }

    private registerSubscription(userId: string, emailLink: string) {
        this.organizationService.getOrganizationByUserId(userId).subscribe(
            (instance: any) => {
                if (instance) {
                    this.register(instance, emailLink);
                }
            }
        );
    }

    public register(data: any, emailLink: string) {
        this.applicationSubscription = new ApplicationSubscription();
        this.applicationSubscription.b4321 = data.b4321;
        this.applicationSubscription.k0871 = this.applicationId;

        this.applicationBySubscriptionService.register(this.applicationSubscription).subscribe(
            (instance: any) => {
                if (instance.status) {
                    this.applicationSubscription.w0791 = instance.data;
                    this.processOrganizationData(data);
                    this.saveOrganizationData(emailLink);
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    private saveOrganizationData(emailLink: string) {
        this.accountPpe.documentType = '1';
        this.accountService.saveAccount(this.accountPpe).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        this.sendWelcomeEmail(emailLink);
                        // this.sendValidationEmail(emailLink);
                        this.router.navigate(['/commons/confirm']).then();
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                } else {
                    this.modalService.warning({
                        nzTitle: 'Warning',
                        nzContent: instance.message
                    });
                }
            }
        );
    }

    private processOrganizationData(data: any) {
        this.accountPpe.organizationId = this.applicationSubscription.w0791;
        // this.accountPpe.userCode = this.firstState.username + '@' + this.firstState.domain;
        this.accountPpe.userCode = this.firstState.email; // usar email hasta definir el campo
        this.accountPpe.userName = data.f4472;
        this.accountPpe.businessAddress = data.f4463 + ' ' + data.f4464;
        this.accountPpe.businessName = data.f4462;
        this.accountPpe.documentType = null; /// dato necesario
        this.accountPpe.documentNumber = data.f4470;
        this.accountPpe.country = 'PE';

        this.accountParameters = new Array<AccountParameter>();

        this.accountAddParameter('email', data.f4471);
        this.accountAddParameter('movil', data.f4469);
        this.accountPpe.parameters = this.accountParameters;
    }

    private accountAddParameter(code: string, value: string) {
        if (value) {
            const parameter = new AccountParameter();
            parameter.code = code;
            parameter.value = value;
            this.accountParameters.push(parameter);
        }
    }

    private sendWelcomeEmail(link: string) {
        const welcomeRequest = {
            to: this.registerService.state1.email,
            subject: EmailSubject.WELCOME,
            template: NotificationTemplate.WELCOME,
            messageData: [
                {
                    code: 'Nombre',
                    value: this.registerService.state4.name
                },
                {
                    code: 'Usuario',
                    // value: this.registerService.state1.username + '@' + this.registerService.state1.domain
                    value: this.registerService.state1.email
                },
                {
                    code: 'Contraseña',
                    value: this.registerService.state1.password
                },
                {
                    code: 'Link',
                    value: this.consoleUrl + '/v?cid=' + link
                }
            ]
        };
        this.notificationService.sendEmail(welcomeRequest).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    // this.sendValidationEmail(link);
                }
            }
        );
    }

    // private sendValidationEmail(link: string) {
    //     const EmailValidationRequest = {
    //         to: this.registerService.state1.email,
    //         subject: EmailSubject.EMAIL_VALIDATION,
    //         template: NotificationTemplate.EMAIL_VALIDATION,
    //         messageData: [
    //             {
    //                 code: 'Usuario',
    //                 value: this.registerService.state1.username + '@' + this.registerService.state1.domain
    //             },
    //             {
    //                 code: 'Link',
    //                 value: this.consoleUrl + '/v?cid=' + link
    //             }
    //         ]
    //     };
    //     this.notificationService.sendEmail(EmailValidationRequest).subscribe(
    //         (instance: any) => {
    //             if (instance.Result === 0) {
    //                 // this.router.navigate(['/commons/confirm']).then();
    //             }
    //         }
    //     );
    // }

    enableButton(): void {
        setTimeout(() => {
            this.disabled = false;
        }, 2000);
    }

    // skip() {
    //   this.registerService.state4 = null;
    //   this.createUser();
    // }

    back() {
        this.router.navigate(['/commons/organization']).then();
    }

    get name() {
        return this.billingDataForm.get('name');
    }
    get email() {
        return this.billingDataForm.get('email');
    }
    get name2() {
        return this.billingDataForm.get('name2');
    }
    get email2() {
        return this.billingDataForm.get('email2');
    }
}
