import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-action',
    templateUrl: './action.component.html',
    styleUrls: ['./action.component.scss']
})
export class ActionComponent implements OnInit {

    private mode: string;
    private actionCode: string;
    private continueUrl: string;
    private lang: string;
    public verifiedEmail = new Subject<boolean>();
    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.queryParamMap.subscribe(params => {
            this.mode = params.get('mode');
            this.actionCode = params.get('oobCode');
            this.continueUrl = params.get('continueUrl');
            this.lang = params.get('lang') || 'en';

            if (this.mode && this.actionCode) {
                this.doAction();
            } else {
                this.router.navigate(['/commons']);
            }
        });
    }

    public doAction() {
        switch (this.mode) {
            case 'resetPassword':
                // // Display reset password handler and UI.
                // handleResetPassword(auth, actionCode, continueUrl, lang);
                break;
            case 'recoverEmail':
                // // Display email recovery handler and UI.
                // handleRecoverEmail(auth, actionCode, lang);
                break;
            case 'verifyEmail':
                // Display email verification handler and UI.
                this.handleVerifyEmail(this.actionCode, this.continueUrl, this.lang);
                break;
            default:
            // Error: invalid mode.
        }
    }

    public handleVerifyEmail(actionCode, continueUrl, lang) {
        firebase.auth().applyActionCode(actionCode).then((resp) => {
            this.verifiedEmail.next(true);
        }).catch((error) => {
            this.verifiedEmail.next(false);
        });
    }

}
