import { Component, OnDestroy, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/adminservices/user.service';
import { NzSizeLDSType, NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { NotificationService } from 'src/app/adminservices/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from 'src/app/adminservices/util.service';
import { ForgotPassword, EmailSubject, NotificationTemplate } from 'src/app/class/enums';
import { AuthenticationType, Constants } from 'src/app/class/constants';

@Component({
    selector: 'app-sendcode',
    templateUrl: './sendcode.component.html',
    styleUrls: ['./sendcode.component.scss']
})
export class SendcodeComponent implements OnInit {
    public userUid: string;
    public userName: string;
    public userEmail: string;
    public userPhone: string;
    public userCode: string;
    public forgotPassword = '1';
    public size: NzSizeLDSType = 'large';
    private nextPage: string[] = ['/commons/forgot/password/code'];
    private loginPage: string[] = ['/commons'];
    constructor(
        private forgotService: ForgotService,
        private userService: UserService,
        private notificationService: NotificationService,
        private utilService: UtilService,
        private nzNotification: NzNotificationService,
        private translate: TranslateService,
        private modalService: NzModalService,
        private router: Router) { }

    ngOnInit() {
        this.userUid = this.forgotService.forgotUser.uid;
        this.userName = this.forgotService.forgotUser.user;
        this.userEmail = this.utilService.hideData(this.forgotService.forgotUser.email);
        this.userPhone = this.utilService.hideData(this.forgotService.forgotUser.movil);

        if (this.userName) {
            if (!this.userEmail || !this.userPhone) {
                this.getUserInfo(this.userName);
            }
        } else {
            this.router.navigate(this.loginPage);
        }
    }

    public getUserInfo(userName: string) {
        const userDto: any = {
            username: userName,
            authType: AuthenticationType.EMAIL,
        };

        this.userService.userInfo(userDto).subscribe(
            (instance: any) => {
                if (instance) {
                    this.forgotService.forgotUser.uid = instance.g6731;
                    this.forgotService.forgotUser.email = instance.email;
                    this.forgotService.forgotUser.movil = instance.phoneNumber;
                    this.userUid = instance.g6731;
                    this.userEmail = this.utilService.hideData(instance.email);
                    this.userPhone = this.utilService.hideData(instance.phoneNumber);
                } else {
                    this.nzNotification.create(
                        'warning',
                        this.translate.instant('MODAL.WARNING'),
                        this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.WARNING_USER')
                    );
                    this.router.navigate(this.loginPage);
                }
            },
            (err) => {
                console.error(err);
                this.router.navigate(this.loginPage);
            },
        );
    }

    public sendCode() {

        this.userCode = String(this.generateValidationCode());
        switch (this.forgotPassword) {
            case ForgotPassword.SEND_EMAIL:
                const emailRequest = {
                    to: this.forgotService.forgotUser.email,
                    subject: EmailSubject.VERIFICATION_CODE,
                    template: NotificationTemplate.VERIFICATION_CODE,
                    messageData: [
                        {
                            code: 'Usuario',
                            value: this.forgotService.forgotUser.user
                        },
                        {
                            code: 'Code',
                            value: Constants.VERIFICATION_CODE_PREFIX + this.userCode
                        }
                    ]
                };
                this.notificationService.sendEmail(emailRequest).subscribe(
                    (instance: any) => {
                        if (instance.Result === 0) {
                            this.forgotService.forgotUser.code = this.userCode;
                            this.nzNotification.create(
                                'success',
                                this.translate.instant('MODAL.SUCCESS'),
                                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.SEND_CODE')
                            );
                            this.router.navigate(this.nextPage);
                        } else {
                            this.modalService.warning({
                                nzTitle: this.translate.instant('MODAL.WARNING'),
                                nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                            });
                        }
                    }
                    // ,
                    // (error: any) => {
                    //     this.router.navigate(this.loginPage);
                    // }
                );
                break;
            case ForgotPassword.SEND_SMS:
                const smsRequest = {
                    to: this.forgotService.forgotUser.movil,
                    subject: '',
                    template: NotificationTemplate.PHONE_VALIDATION,
                    messageData: [
                        {
                            code: 'Codigo',
                            value: Constants.VERIFICATION_CODE_PREFIX + this.userCode
                        }
                    ]
                };
                this.notificationService.sendSms(smsRequest).subscribe(
                    (instance: any) => {
                        if (instance.Result === 0) {
                            this.forgotService.forgotUser.code = this.userCode;
                            this.nzNotification.create(
                                'success',
                                this.translate.instant('MODAL.SUCCESS'),
                                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.SEND_CODE')
                            );
                            this.router.navigate(this.nextPage);
                        } else {
                            this.modalService.warning({
                                nzTitle: this.translate.instant('MODAL.WARNING'),
                                nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                            });
                        }
                    }
                    // ,
                    // (error: any) => {
                    //     this.router.navigate(this.loginPage);
                    // }
                );
                break;
            default:
                break;
        }
    }

    public back() {
        this.forgotService.clearForgotUser();
        this.router.navigate(this.loginPage);
    }

    private generateValidationCode() {
        return Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000;
    }
}
