import { Component, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NzSizeLDSType, NzModalService } from 'ng-zorro-antd';
import { MaxLength, MinLength } from 'src/app/class/constants';
import { UserService } from 'src/app/adminservices/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-restore-password',
    templateUrl: './restore-password.component.html',
    styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent implements OnInit {

    MaxLength: any = MaxLength;
    public userUid: string;
    public userValid: boolean;
    public userName: string;
    public restoreForm: FormGroup;
    public passwordVisible = false;
    public inputSize: NzSizeLDSType = 'large';
    private nextPage: string[] = ['/commons/forgot/password/confirm'];
    private lastPage: string[] = ['/commons/forgot/password/code'];
    private loginPage: string[] = ['/commons'];

    constructor(
        private fb: FormBuilder,
        private forgotService: ForgotService,
        private userService: UserService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userUid = this.forgotService.forgotUser.uid;
        this.userValid = this.forgotService.forgotUser.valid;
        this.userName = this.forgotService.forgotUser.user;
        this.createRestoreForm();
        if (!this.userValid) {
            this.router.navigate(this.loginPage);
        }
    }

    public createRestoreForm() {
        this.restoreForm = this.fb.group({
            password: [null, [Validators.required, Validators.minLength(MinLength.PASSWORD)]],
            checkPassword: [null, [Validators.required, Validators.minLength(MinLength.PASSWORD), this.confirmPassword]]
        });
    }

    confirmPassword = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { error: true, required: true };
        } else if (control.value !== this.restoreForm.controls.password.value) {
            return { confirm: true, error: true };
        }
        return {};
    }

    updateConfirmValidator(): void {
        Promise.resolve().then(() => this.checkPassword.updateValueAndValidity());
    }    

    goConfirmPassword() {
        this.userService.userChangePass(this.userUid, this.password.value).subscribe(
            (instance: any) => {
                if (instance.status) {
                    this.forgotService.forgotUser.restored = true;
                    this.router.navigate(this.nextPage);
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public back() {
        this.forgotService.forgotUser.valid = false;
        this.router.navigate(this.lastPage);
    }

    get password() {
        return this.restoreForm.get('password');
    }
    get checkPassword() {
        return this.restoreForm.get('checkPassword');
    }
}
