import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecuritycodeComponent } from './securitycode.component';

describe('SecuritycodeComponent', () => {
  let component: SecuritycodeComponent;
  let fixture: ComponentFixture<SecuritycodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecuritycodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecuritycodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
