import { Component, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaxLength } from 'src/app/class/constants';
import { NzSizeLDSType, NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-securitycode',
    templateUrl: './securitycode.component.html',
    styleUrls: ['./securitycode.component.scss']
})
export class SecuritycodeComponent implements OnInit {

    MaxLength: any = MaxLength;
    public userCode: string;
    public userName: string;
    public codeForm: FormGroup;
    public inputSize: NzSizeLDSType = 'large';
    private nextPage: string[] = ['/commons/forgot/password/restore'];
    private lastPage: string[] = ['/commons/forgot/password'];
    private loginPage: string[] = ['/commons'];

    constructor(
        private forgotService: ForgotService,
        private fb: FormBuilder,
        private router: Router,
        private nzNotification: NzNotificationService,
        private translate: TranslateService
    ) { }

    ngOnInit() {

        this.userCode = this.forgotService.forgotUser.code;
        this.userName = this.forgotService.forgotUser.user;

        this.createCodeForm();
        if (!this.userCode) {
            this.router.navigate(this.loginPage);
        }
    }

    public createCodeForm() {

        this.codeForm = this.fb.group({
            code: [null, [Validators.required]]
        });

    }

    public goRestorePassword() {
        if (this.codeValidate()) {
            this.forgotService.forgotUser.valid = true;
            this.router.navigate(this.nextPage);
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.WARNING_CODE')
            );
        }
    }

    private codeValidate(): boolean {
        return this.code.value === this.userCode ? true : false;
    }

    getPhoneCodeRegExp(code: string): RegExp {
        return code ? new RegExp('^' + code + '$') : null;
    }   

    public back() {
        this.router.navigate(this.lastPage);
    }

    get code() {
        return this.codeForm.get('code');
    }
}
