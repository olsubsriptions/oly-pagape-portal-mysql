import { Component, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzSizeLDSType, NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { MaxLength } from 'src/app/class/constants';
import { UserService } from 'src/app/adminservices/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-restore-user',
    templateUrl: './restore-user.component.html',
    styleUrls: ['./restore-user.component.scss']
})
export class RestoreUserComponent implements OnInit {

    MaxLength: any = MaxLength;
    public userUid: string;
    public userName: string;
    public userEmail: string;
    public restoreForm: FormGroup;
    public inputSize: NzSizeLDSType = 'large';
    public loginPage: string[] = ['/commons'];
    public nextPage: string[] = ['/commons/restore/user/confirm'];

    constructor(
        private forgotService: ForgotService,
        private router: Router,
        private userService: UserService,
        private nzNotification: NzNotificationService,
        private translate: TranslateService,
        private modalService: NzModalService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.createRestoreForm();
        this.userUid = this.forgotService.forgotUser.uid;
        this.userName = this.forgotService.forgotUser.user;
        if (!this.userName) {
            this.router.navigate(this.loginPage);
        } else {
            this.getUserInfo(this.userName);
        }
    }

    public createRestoreForm() {
        this.restoreForm = this.formBuilder.group({
            email: [null, [Validators.required, Validators.minLength(4), Validators.email]],
        });
    }

    public getUserInfo(userName: string) {
        this.userService.userInfo(userName).subscribe(
            (instance: any) => {
                if (instance) {
                    this.forgotService.forgotUser.uid = instance.g6731;
                    this.forgotService.forgotUser.email = instance.email;
                    this.userUid = instance.g6731;
                    this.userEmail = instance.email;
                } else {
                    this.nzNotification.create(
                        'warning',
                        this.translate.instant('MODAL.WARNING'),
                        this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.WARNING_USER')
                    );
                    this.router.navigate(this.loginPage);
                }
            },
            (err) => {
                console.error(err);
                this.router.navigate(this.loginPage);
            },
        );
    }    

    public goConfirmUser() {
        if (this.emailValidate()) {
            this.forgotService.forgotUser.valid = true;
            this.userService.userUnlock(this.userUid).subscribe(
                (instance: any) => {
                    if (instance.status) {
                        this.forgotService.forgotUser.restored = true;
                        this.router.navigate(this.nextPage);
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            );
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.WARNING_EMAIL')
            );
        }
    }

    private emailValidate(): boolean {
        return this.email.value === this.userEmail ? true : false;
    }

    get email() {
        return this.restoreForm.get('email');
    }
}
