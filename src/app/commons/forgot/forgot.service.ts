import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ForgotService {
    apiUrl = environment.api.url;
    public forgotUser = {
        uid: null,
        user: null,
        pass: null,
        email: null,
        movil: null,
        code: null,
        valid: null,
        restored: null
    };
    constructor() {
    }

    public clearForgotUser(){
        this.forgotUser.uid = null;
        this.forgotUser.user = null;
        this.forgotUser.pass = null;
        this.forgotUser.email = null;
        this.forgotUser.movil = null;
        this.forgotUser.code = null;
        this.forgotUser.valid = null;
        this.forgotUser.restored = null;
    }
}
