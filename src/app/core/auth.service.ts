import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UserFb } from '../class/userFb';
import { ApplicationSubscription } from '../class/applicationSubscription';
import * as firebase from 'firebase/app';
import { LoaderService } from '../adminservices/loader.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    user: UserFb = null;
    suscriptionState = false;
    applicationSuscription: ApplicationSubscription;

    constructor(
        public angularFireAuth: AngularFireAuth,
        public loaderService: LoaderService,
        public router: Router) {
    }

    login(email: string, password: string) {
        this.loaderService.isLoading.next(true);
        return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
    }

    loginGoogle() {
        const provider = new firebase.auth.GoogleAuthProvider();
        return firebase.auth().signInWithPopup(provider);
    }

    async logout() {
        this.loaderService.isLoading.next(true);
        this.user = null;
        this.suscriptionState = false;
        this.applicationSuscription = null;
        await this.angularFireAuth.auth.signOut();
    }
    get authenticated(): boolean {
        return this.user !== null;
    }

    public getSuscriptionState(): boolean {
        return this.suscriptionState;
    }

    public setSuscriptionState(suscriptionState: boolean) {
        this.suscriptionState = suscriptionState;
    }

    public getApplicationSuscription(): ApplicationSubscription {
        return this.applicationSuscription;
    }

    public setApplicationSuscription(applicationSuscription: ApplicationSubscription) {
        this.applicationSuscription = applicationSuscription;
    }

    public getUser(): UserFb {
        return this.user;
    }

    public setUser(user: UserFb) {
        if (user) {
            this.user = user;
        } else {
            this.user = null;
        }
    }
}
