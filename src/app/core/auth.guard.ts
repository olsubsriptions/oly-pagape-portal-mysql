import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import * as firebase from 'firebase/app';
import { environment } from 'src/environments/environment';
import { ApplicationBySubscriptionService } from '../adminservices/applicationBySubscription.service';
import { Constants } from '../class/constants';
import { UserFb } from '../class/userFb';
import { ApplicationSubscription } from '../class/applicationSubscription';
import { MenuService } from '../adminservices/menu.service';
// import 'firebase/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    private landingPage = environment.landingPage.url;
    public pagapeShortName = Constants.pagapeCode;
    public user: UserFb;

    constructor(
        private authService: AuthService,
        private menuService: MenuService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise((resolve, reject) => {
            this.menuService.setSelectedMenu(state.url);
            firebase.auth().onAuthStateChanged((user: firebase.User) => {
                if (user) {
                    this.user = new UserFb();
                    this.user.displayName = user.displayName;
                    this.user.email = user.email;
                    this.user.emailVerified = user.emailVerified;
                    this.user.photoURL = user.photoURL;
                    this.user.uid = user.uid;
                    this.user.providerData = user.providerData;
                    user.getIdToken().then(token => {
                        this.user.token = token;
                        this.authService.setUser(this.user);
                        this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
                            (instance: any) => {
                                if (instance) {
                                    // const applicationSuscription = instance.find(x => x.w0799 === this.pagapeShortName);
                                    const applicationSuscription = instance.find(x => x.k0873 === this.pagapeShortName);
                                    this.authService.setApplicationSuscription(applicationSuscription);
                                    this.authService.setSuscriptionState(applicationSuscription ? true : false);
                                }
                                if (this.authService.getSuscriptionState()) {
                                    resolve(true);
                                } else {
                                    const urlTree = this.router.createUrlTree(['/admin/subscription']); //probar
                                    resolve(urlTree);
                                }
                            }
                        );
                    });
                } else {
                    this.authService.setUser(null);
                    window.location.href = this.landingPage;
                    resolve(false);
                }
            });
        });
    }
}
