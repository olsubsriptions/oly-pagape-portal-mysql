import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Constants } from '../class/constants';
import { UserFb } from '../class/userFb';
import * as firebase from 'firebase/app';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class AuthGuardSub implements CanActivate {

    private landingPage = environment.landingPage.url;
    public pagapeShortName = Constants.pagapeCode;
    public user: UserFb;
    constructor(
        private authService: AuthService) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged((user: firebase.User) => {
                if (user) {
                    this.user = new UserFb();
                    this.user.displayName = user.displayName;
                    this.user.email = user.email;
                    this.user.emailVerified = user.emailVerified;
                    this.user.photoURL = user.photoURL;
                    this.user.uid = user.uid;
                    this.user.providerData = user.providerData;
                    user.getIdToken().then(token => {
                        this.user.token = token;
                        this.authService.setUser(this.user);
                        resolve(true);
                    });
                } else {
                    this.authService.setUser(null);
                    window.location.href = this.landingPage;
                    resolve(false);
                }
            });
        });
    }
}
