import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http';
import { Observable, EMPTY, throwError, of } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd';
import { LoaderService } from '../adminservices/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthService } from '../core/auth.service';
import { CryptoService } from '../adminservices/crypto.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private landingPage = environment.landingPage.url;
    public user: any;
    public token: string;

    private requests: HttpRequest<any>[] = [];

    constructor(
        private authService: AuthService,
        private modalService: NzModalService,
        private loaderService: LoaderService,
        private cryptoService: CryptoService,
        private translate: TranslateService) {
    }

    removeRequest(request: HttpRequest<any>) {
        const i = this.requests.indexOf(request);
        if (i >= 0) {
            this.requests.splice(i, 1);
        }
        this.loaderService.isLoading.next(this.requests.length > 0);
    }

    private applyFirebaseCredentials = (request: HttpRequest<any>, token: string) => {
        return request.clone({
            setHeaders: {
                Authorization: 'Bearer ' + token
            }
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.user = this.authService.getUser();

        if (this.user) {
            this.token = this.user.token;
            request = this.applyFirebaseCredentials(request, this.token);
        }

        this.requests.push(request);
        this.loaderService.isLoading.next(true);

        // const newBody = {
        //     id: 1,
        //     data: this.cryptoService.aesEncrypt(JSON.stringify(request.body))
        // }
        // const newRequest = request.clone({ body: newBody});

        // console.log('REQUEST => ', newRequest.body);
        // console.log('REQUEST_JSON_STRING => ', JSON.stringify(newRequest.body));

        console.log('REQUEST => ', request.body);
        console.log('REQUEST_JSON_STRING => ', JSON.stringify(request.body));

        return next.handle(request).pipe(
            map(event => {
                if (event instanceof HttpResponse) {
                    this.removeRequest(request);
                    console.log('RESPONSE => ', event.body);
                    console.log('RESPONSE_JSON_STRING => ', JSON.stringify(event.body));
                    if (request.url.includes('.json')) {
                        return event;
                    } else {
                        // const newEvent = event.clone({ body: this.cryptoService.aesEncrypt(JSON.stringify(event.body)) });
                        return event;
                    }
                }
            }),
            catchError(error => {
                if (error.status === 401) {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('INTERCEPTOR.ATENTION'),
                        nzContent: this.translate.instant('INTERCEPTOR.NOT_AUTHORIZED'),
                    });
                    this.removeRequest(request);
                    window.location.href = this.landingPage;
                    console.log(error.error.message);
                    return EMPTY;
                } else {
                    this.modalService.error({
                        nzTitle: this.translate.instant('INTERCEPTOR.ERROR'),
                        nzContent: this.translate.instant('INTERCEPTOR.FATAL_ERROR'),
                    });
                    this.removeRequest(request);
                    console.log(error.error.message ? error.error.message : error.name);
                    return throwError(error);
                }
            })
        );
    }
}
