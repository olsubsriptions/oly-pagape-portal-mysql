import { Directive, HostListener } from '@angular/core';
import { RegExpresion } from '../class/constants';

@Directive({
    selector: '[appPassword]'
})
export class PasswordDirective {

    constructor() { }

    @HostListener('keypress', ['$event']) onkeypress(event: KeyboardEvent) {

        if (!event.key.match(RegExpresion.PASSWORD)) {
            event.preventDefault();
        }
    }

    @HostListener('paste', ['$event']) onpaste(event: ClipboardEvent) {
        const pasteValue = event.clipboardData.getData('text');

        Array.from(pasteValue).forEach(key => {
            if (!key.match(RegExpresion.PASSWORD)) {
                event.preventDefault();
                return;
            }
        });
    }
}
