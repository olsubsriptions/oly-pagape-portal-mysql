import { Directive, HostListener } from '@angular/core';
import { RegExpresion } from '../class/constants';

@Directive({
    selector: '[appCompany]'
})
export class CompanyDirective {

    constructor() { }

    @HostListener('keypress', ['$event']) onkeypress(event: KeyboardEvent) {

        if (!event.key.match(RegExpresion.COMPANY)) {
            event.preventDefault();
        }
    }

    @HostListener('paste', ['$event']) onpaste(event: ClipboardEvent) {
        const pasteValue = event.clipboardData.getData('text');

        Array.from(pasteValue).forEach(key => {
            if (!key.match(RegExpresion.COMPANY)) {
                event.preventDefault();
                return;
            }
        });
    }
}
