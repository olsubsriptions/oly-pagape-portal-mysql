import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[appMaxLength]'
})
export class MaxLengthDirective {

    @Input('appMaxLength') maxLength: number;
    // example  [appMaxLength]=8
    // example  [appMaxLength]="constants.PSSWRD_MAX_LENGTH"
    constructor() {
    }

    @HostListener('keypress', ['$event']) onkeypress(event) {
        const selectedLength = window.getSelection().toString().length;
        if (event.target.value.length - selectedLength >= this.maxLength) {
            event.preventDefault();
        }
    }

    @HostListener('paste', ['$event']) onpaste(event) {
        const currentValue = event.target.value;
        const pasteValue = event instanceof ClipboardEvent ? event.clipboardData.getData('text') : '';
        const selectedLength = window.getSelection().toString().length;
        
        if ((currentValue.length - selectedLength + pasteValue.length) > this.maxLength) {
            event.preventDefault();
        }
    }
}
