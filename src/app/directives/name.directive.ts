import { Directive, HostListener } from '@angular/core';
import { RegExpresion } from '../class/constants';

@Directive({
    selector: '[appName]'
})
export class NameDirective {

    constructor() { }

    @HostListener('keypress', ['$event']) onkeypress(event: KeyboardEvent) {

        if (!event.key.match(RegExpresion.NAME)) {
            event.preventDefault();
        }
    }

    @HostListener('paste', ['$event']) onpaste(event: ClipboardEvent) {
        const pasteValue = event.clipboardData.getData('text');

        Array.from(pasteValue).forEach(key => {
            if (!key.match(RegExpresion.NAME)) {
                event.preventDefault();
                return;
            }
        });
    }
}
