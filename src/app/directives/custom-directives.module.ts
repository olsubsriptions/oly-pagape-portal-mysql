import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutofocusDirective } from './autofocus.directive';
import { MaxLengthDirective } from './max-length.directive';
import { MailDirective } from './mail.directive';
import { PasswordDirective } from './password.directive';
import { NumberDirective } from './number.directive';
import { CompanyDirective } from './company.directive';
import { LocationDirective } from './location.directive';
import { AddressDirective } from './address.directive';
import { NameDirective } from './name.directive';
import { PhoneNumberDirective } from './phone-number.directive';
import { PaymentDirective } from './payment.directive';
import { AmountDirective } from './amount.directive';



@NgModule({
    declarations: [
        AutofocusDirective,
        MaxLengthDirective,
        MailDirective,
        PasswordDirective,
        NumberDirective,
        CompanyDirective,
        LocationDirective,
        AddressDirective,
        NameDirective,
        PhoneNumberDirective,
        PaymentDirective,
        AmountDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        AutofocusDirective,
        MaxLengthDirective,
        MailDirective,
        PasswordDirective,
        NumberDirective,
        CompanyDirective,
        LocationDirective,
        AddressDirective,
        NameDirective,
        PhoneNumberDirective,
        PaymentDirective,
        AmountDirective]
})
export class CustomDirectivesModule { }
