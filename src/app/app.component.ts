import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ngx-os-portal';

  constructor(private translate: TranslateService) {
  }
  ngOnInit(): void {
    this.translate.use('es');
    localStorage.setItem('portal-curr-lang', 'es');
  }
}
