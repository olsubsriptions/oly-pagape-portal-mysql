import { Pipe, PipeTransform } from '@angular/core';
import { UtilService } from '../adminservices/util.service';

@Pipe({
  name: 'currencyNumberToWord'
})
export class CurrencyNumberToWordPipe implements PipeTransform {

  constructor(
    private utilService: UtilService
  ) {

  }

  transform(value: any): any {
    if (value === undefined) {
      return null;
    }
    return this.utilService.currencyNumberToWord(value);
  }

}
