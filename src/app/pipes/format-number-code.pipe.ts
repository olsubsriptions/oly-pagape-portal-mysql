import { Pipe, PipeTransform } from '@angular/core';
import { UtilService } from '../adminservices/util.service';

@Pipe({
  name: 'formatNumberCode'
})
export class FormatNumberCodePipe implements PipeTransform {

  constructor(
    private utilService: UtilService
  ) {

  }
  transform(value: string, ...args: any[]): any {
    if (value === undefined || value === null) {
      return null;
    }
    return 'PE-' + this.utilService.formatNumber(value, args[0]);
  }

}
