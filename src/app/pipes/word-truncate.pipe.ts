import { Pipe, PipeTransform } from '@angular/core';
import { UtilService } from '../adminservices/util.service';

@Pipe({
  name: 'wordTruncate'
})
export class WordTruncatePipe implements PipeTransform {

  constructor(
    private utilService: UtilService
  ) {

  }

  transform(value: string, ...args: any[]): any {
    if (value === undefined) {
      return null;
    }
    if (value.length > args[0]) {
      return this.utilService.wordTruncate(value, args[0]);
    }
    return value;
  }

}
