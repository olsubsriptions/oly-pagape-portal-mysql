import { Pipe, PipeTransform } from '@angular/core';
import { UtilService } from '../adminservices/util.service';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  constructor(
    private utilService: UtilService
  ) {

  }
  transform(value: string, ...args: any[]): any {
    if (value === undefined) {
      return null;
    }
    return this.utilService.formatNumber(value, args[0]);
  }

}
