import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatNumberPipe } from './format-number.pipe';
import { OrdinalNumberToWordPipe } from './ordinal-number-to-word.pipe';
import { FormatCurrencyPipe } from './format-currency.pipe';
import { CurrencyNumberToWordPipe } from './currency-number-to-word.pipe';
import { FormatNumberCodePipe } from './format-number-code.pipe';
import { WordTruncatePipe } from './word-truncate.pipe';



@NgModule({
  declarations: [
    FormatNumberPipe,
    OrdinalNumberToWordPipe,
    FormatCurrencyPipe,
    CurrencyNumberToWordPipe,
    FormatNumberCodePipe,
    WordTruncatePipe],
  imports: [
    CommonModule
  ],
  exports: [
    FormatNumberPipe,
    OrdinalNumberToWordPipe,
    FormatCurrencyPipe,
    CurrencyNumberToWordPipe,
    FormatNumberCodePipe,
    WordTruncatePipe]
})
export class CustomPipesModule { }
