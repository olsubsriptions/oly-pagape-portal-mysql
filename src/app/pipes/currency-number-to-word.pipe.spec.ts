import { UtilService } from '../adminservices/util.service';
import { CurrencyNumberToWordPipe } from './currency-number-to-word.pipe';

describe('CurrencyNumberToWordPipe', () => {
  it('create an instance', () => {
    const pipe = new CurrencyNumberToWordPipe(new UtilService());
    expect(pipe).toBeTruthy();
  });
});
