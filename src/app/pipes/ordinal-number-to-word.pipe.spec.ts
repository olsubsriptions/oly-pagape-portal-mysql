import { UtilService } from '../adminservices/util.service';
import { OrdinalNumberToWordPipe } from './ordinal-number-to-word.pipe';

describe('OrdinalNumberToWordPipe', () => {
  it('create an instance', () => {
    const pipe = new OrdinalNumberToWordPipe(new UtilService());
    expect(pipe).toBeTruthy();
  });
});
