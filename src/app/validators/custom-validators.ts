import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Constants, RegExpresion, ValidatorRegExpresion } from '../class/constants';

export class CustomValidators {

    static password(control: AbstractControl): ValidationErrors | null {
        if (!control.value || control.value.match(RegExpresion.COMMONS_PSSWRD)) {
            return null;
        }
        return { password: true };
    }

    static password2(val: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            if (!control.value || control.value.match(RegExpresion.COMMONS_PSSWRD)) {
                return null;
            }
            return { password: true };
        };
    }

    static phoneNumber(control: AbstractControl): ValidationErrors | null {
        if (!control.value || control.value.match(ValidatorRegExpresion.PHONE_NUMBER)) {
            return null;
        }
        return { phoneNumber: true };
    }

    static currencyAmount(control: AbstractControl): ValidationErrors | null {
        if (!control.value ||
            (control.value.match(ValidatorRegExpresion.CURRENCY_AMOUNT) &&
                parseFloat(control.value) < Constants.AMOUNT_MAX_VALUE)) {
            return null;
        }
        return { currencyAmount: true };
    }
}



// function smsValidate(code: RegExp): ValidatorFn {
//     return (control: AbstractControl): { [key: string]: boolean } | null => {
//         if (code && control.value && control.value.match(code)) {
//             return null;
//         }
//         return { smsValidate: true };
//     };
// }