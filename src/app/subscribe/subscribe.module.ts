import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubscribeRoutingModule } from './subscribe-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SubscribeRoutingModule
  ]
})
export class SubscribeModule { }
