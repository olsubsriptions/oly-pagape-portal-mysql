import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { MenuTypes } from '../class/enums';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionService } from '../adminservices/subscription.service';
import { ApplicationSubscription } from '../class/applicationSubscription';
import { ApplicationBySubscriptionService } from '../adminservices/applicationBySubscription.service';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { Constants } from '../class/constants';
import { AccountService } from '../adminservices/account.service';
import { AccountPpe } from '../class/accountPpe';
import { MenuService } from '../adminservices/menu.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

    public menuLogout;
    public menuItems;
    public menuTypes = MenuTypes;
    public selectedLanguage: string;
    public applicationSubscription: ApplicationSubscription;
    public user: any;
    public uMail: any;
    public applicationId = '2df6ba93-9de1-4737-8cfd-2e7c33cd3271';
    public pagapeShortName = Constants.pagapeCode;
    public accountPpe = new AccountPpe();
    isCollapsed = false;

    constructor(
        private router: Router,
        private modalService: NzModalService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private subscriptionService: SubscriptionService,
        private authService: AuthService,
        private accountService: AccountService,
        private menuService: MenuService,
        private translate: TranslateService) {

        this.getLanguage();
    }

    ngOnInit() {
        this.menuItems = this.menuService.getMenuItems();
        this.menuLogout = this.menuService.getMenuLogout();
        this.user = this.authService.getUser();
        this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
            (instance: any) => {
                if (instance) {
                    // this.applicationSubscription = instance.find(x => x.w0799 === this.pagapeShortName);
                    this.applicationSubscription = instance.find(x => x.k0873 === this.pagapeShortName);
                    if (this.applicationSubscription) {
                        this.accountPpe.organizationId = this.applicationSubscription.w0791;
                        this.accountPpe.userCode = this.user.displayName;
                        this.getPpeAccount(this.accountPpe.organizationId);
                    } else {
                        console.log('Suscribete');
                    }
                } else {
                    console.log('Suscribete');
                }
            }
        );
    }

    public getPpeAccount(appSubscriptionId: string) {
        this.accountService.getAccount(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.accountPpe = instance.Data;
                            this.uMail = this.accountPpe.parameters.find(x => x.code === 'email').value;
                        } else {
                            // this.getOrganizationByUserId(this.user.uid);
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                    }
                }
            }
        );
    }

    logout() {
        this.authService.logout();
    }

    changeLanguage() {
        if (this.selectedLanguage === 'es') {
            this.translate.use('es');
            localStorage.setItem('portal-curr-lang', 'es');
        } else {
            this.translate.use('en');
            localStorage.setItem('portal-curr-lang', 'en');
        }
    }

    getLanguage() {
        if (localStorage.getItem('portal-curr-lang')) {
            this.translate.setDefaultLang(localStorage.getItem('portal-curr-lang'));
            this.translate.use(localStorage.getItem('portal-curr-lang'));
        } else {
            this.translate.setDefaultLang('es');
            this.translate.use('es');
        }
        this.selectedLanguage = this.translate.getDefaultLang();
    }
    public register() {
        this.subscriptionService.getSubscriptionByUserId(this.user.uid).subscribe(
            (subscription: any) => {
                this.applicationSubscription = new ApplicationSubscription();
                this.applicationSubscription.b4321 = subscription.b4321;
                this.applicationSubscription.k0871 = this.applicationId;

                this.applicationBySubscriptionService.register(this.applicationSubscription).subscribe(
                    (instance: any) => {
                        if (instance.status) {
                            this.modalService.success({
                                nzTitle: this.translate.instant('MODAL.SUCCESS'),
                                nzContent: this.translate.instant('MODAL.SUCCESS_MESSAGE')
                                // nzContent: 'La operación se ha realizado exitosamente'
                            });
                            this.router.navigate(['/admin']);
                        } else {
                            this.modalService.warning({
                                nzTitle: this.translate.instant('MODAL.WARNING'),
                                nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                                // nzContent: 'La operación se ha realizado exitosamente'
                            });
                        }
                    }
                );
            }
        );
    }
}
