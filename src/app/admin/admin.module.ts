import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin.component';
import {AdminRoutingModule} from './admin-routing.module';
import {SharedModule} from '../core/shared.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    TranslateModule.forChild()    
  ]
})
export class AdminModule {
}
