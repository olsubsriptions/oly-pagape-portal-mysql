import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { PaymentComponent } from './payment/payment.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { CustomPipesModule } from 'src/app/pipes/custom-pipes.module';


@NgModule({
  declarations: [PaymentComponent],
  imports: [
    CommonModule,
    BillingRoutingModule,
    NgZorroAntdModule,
    TranslateModule.forChild(),
    CustomPipesModule
  ]
})
export class BillingModule { }
