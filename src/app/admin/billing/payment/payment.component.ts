import { Component, OnInit } from '@angular/core';
import { Pagination } from 'src/app/class/pagination';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { PaymentService } from 'src/app/adminservices/payment.service';
import { NzModalService } from 'ng-zorro-antd';
import { Constants } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { PaymentsStats } from 'src/app/class/paymentsStats';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

    public user: any;
    public paginationPen: Pagination;
    public paginationUsd: Pagination;
    public paymentList: any;
    public pagapeShortName = Constants.pagapeCode;
    public regxpag = Constants.regxpag;
    public applicationSuscription: ApplicationSubscription;
    public organizationId: string;
    public penLastMonth = 0;
    public usdLastMonth = 0;
    public transactionPenList = null;
    public transactionUsdList = null;
    public paymentsStats = new PaymentsStats();
    constructor(
        private translate: TranslateService,
        private paymentService: PaymentService,
        private modalService: NzModalService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.paginationPen = new Pagination(1, this.regxpag, 0);
        this.paginationUsd = new Pagination(1, this.regxpag, 0);
        this.applicationSuscription = this.authService.getApplicationSuscription();
        this.organizationId = this.applicationSuscription.w0791;
        this.getPenTransactions(this.organizationId);
        this.getUsdTransactions(this.organizationId);
        this.getPaymentsStats(this.organizationId);
    }

    public getPenTransactions(appSubscriptionId: string) {
        const request = {
            organizationId: appSubscriptionId,
            currency: Constants.PEN_CODE,
            batch: 0,
            lastMonth: this.penLastMonth,
            pag: this.paginationPen.pag,
            regxpag: this.paginationPen.regxpag
        };

        this.paymentService.getTransactions(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.transactionPenList = instance.Data.QData;
                        this.paginationPen.totalrows = instance.Data.RowCount;
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getUsdTransactions(appSubscriptionId: string) {
        const request = {
            organizationId: appSubscriptionId,
            currency: Constants.USD_CODE,
            batch: 0,
            lastMonth: this.usdLastMonth,
            pag: this.paginationUsd.pag,
            regxpag: this.paginationUsd.regxpag
        };
        this.paymentService.getTransactions(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.transactionUsdList = instance.Data.QData;
                        this.paginationUsd.totalrows = instance.Data.RowCount;
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getPaymentsStats(appSubscriptionId: string) {
        this.paymentService.getPaymentsStats(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.paymentsStats = instance.Data;
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public paginationPenChange(event: Event) {
        this.paginationPen.pag = Number(event);
        this.getPenTransactions(this.organizationId);
    }

    public paginationUsdChange(event: Event) {
        this.paginationUsd.pag = Number(event);
        this.getUsdTransactions(this.organizationId);
    }

    public getPen(flagLastMonth: number) {
        if (this.penLastMonth !== flagLastMonth) {
            this.penLastMonth = flagLastMonth;
            this.getPenTransactions(this.organizationId);
        }
    }

    public getUsd(flagLastMonth: number) {
        if (this.usdLastMonth !== flagLastMonth) {
            this.usdLastMonth = flagLastMonth;
            this.getUsdTransactions(this.organizationId);
        }

    }
}
