import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenerateRequirementComponent } from './genera-requerimiento/generate-requeriment.component';
import { RequestComponent } from './request/request.component';
import { SupportComponent } from './support/support.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'support'
  },
  {
    path: 'generaterequeriments',
    component: GenerateRequirementComponent
  },
  {
    path: 'requests',
    component: RequestComponent
  },
  {
    path: 'support',
    component: SupportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
