import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateRequirementComponent } from './genera-requerimiento/generate-requeriment.component';
import { SupportRoutingModule } from './support-routing.module';
import { RequestComponent } from './request/request.component';
import { SupportComponent } from './support/support.component';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [GenerateRequirementComponent, RequestComponent, SupportComponent],
  imports: [
    CommonModule,
    SupportRoutingModule,
    FormsModule,
    NgZorroAntdModule,
    TranslateModule.forChild()
  ]
})
export class SupportModule { }
