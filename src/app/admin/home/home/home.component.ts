import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/core/auth.service';
import { Constants } from 'src/app/class/constants';
import { AccountPpe } from 'src/app/class/accountPpe';
import { AccountService } from 'src/app/adminservices/account.service';
import { PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';
import { ChartType } from 'chart.js';
import { PaymentService } from 'src/app/adminservices/payment.service';
import { TransactionInfo } from 'src/app/class/transactionInfo';

interface Person {
    key: string;
    name: string;
    age: number;
    address: string;
}

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public test: any;
    public user: any;
    public applicationId: string;
    public applicationSubscription: ApplicationSubscription;
    public paymentlinkForm: FormGroup;
    public pagapeShortName = Constants.pagapeCode;
    public accountPpe = new AccountPpe();
    public lastMonth = 1;

    // public lineChartOptions = {
    //     scaleShowVerticalLines: false,
    //     responsive: true,
    //     // scales: {
    //     //     xAxes: [{
    //     //         gridLines: {
    //     //             drawOnChartArea: false
    //     //         }
    //     //     }],
    //     //     yAxes: [{
    //     //         gridLines: {
    //     //             drawOnChartArea: false
    //     //         }
    //     //     }]
    //     // }
    // };
    // public lineChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    // public lineChartType = 'line';
    // public lineChartLegend = true;
    // public lineChartData = [
    //     { data: [65, 59, 80, 81, 56, 55, 40], label: 'PEN', lineTension: 0, fill: false },
    //     { data: [28, 48, 40, 19, 86, 27, 90], label: 'USD', lineTension: 0, fill: false }
    // ];
    // public lineChartLabels: any;
    // public lineChartData: any;
    // public lineLabels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    // public lineData = [
    //     { data: [65, 59, 80, 81, 56, 55, 40, 50, 60, 70], label: 'PEN', lineTension: 0, fill: false },
    //     { data: [28, 48, 40, 19, 86, 27, 90, 85, 95, 75], label: 'USD', lineTension: 0, fill: false }
    // ];


    public barChartOptions = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            xAxes: [{
                gridLines: {
                    drawOnChartArea: false
                }
            }],
            // yAxes: [{
            //     gridLines: {
            //         drawOnChartArea: false
            //     }
            // }]
        }
    };
    public barChartLabels = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'];
    public barChartType = 'bar';
    public barChartLegend = false;
    public barChartColors = [
        {
            backgroundColor: 'rgba(82, 140, 245, 1)',
            borderColor: '#fff',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#fff'
        },
        {
            backgroundColor: 'rgba(0, 255, 187, 1)',
            borderColor: '#fff',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#fff'
        }
    ];
    public barChartData = [
        {
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            // label: 'Soles',
            label: Constants.PEN_CODE
        },
        {
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            // label: 'Dólares'
            label: Constants.USD_CODE
        }
    ];


    public doughnutColors = [
        {
            backgroundColor: [
                'rgba(82, 140, 245, 1)',
                'rgba(0, 255, 187, 1)'
            ]
        }
    ];
    public doughnutData = [];
    public doughnutLabels = [];
    public doughnutChartType: ChartType = 'doughnut';
    public doughnutChartLegend = false;
    public doughnutChartOptions = {
        cutoutPercentage: 75
    };
    public doughnutChartPlugins: PluginServiceGlobalRegistrationAndOptions[];


    years: Array<number>;
    listOfData: Person[] = [
        {
            key: '1',
            name: 'John Brown',
            age: 32,
            address: 'New York No. 1 Lake Park'
        },
        {
            key: '2',
            name: 'Jim Green',
            age: 42,
            address: 'London No. 1 Lake Park'
        },
        {
            key: '3',
            name: 'Joe Black',
            age: 32,
            address: 'Sidney No. 1 Lake Park'
        },
        {
            key: '4',
            name: 'Jim Green',
            age: 42,
            address: 'London No. 1 Lake Park'
        },
        {
            key: '5',
            name: 'Joe Black',
            age: 32,
            address: 'Sidney No. 1 Lake Park'
        }
    ];

    public organizationId: string;
    public transactionInfo = new TransactionInfo();
    public accountCurrencyList = new Array<string>();
    public selectedCurrency: string;
    public selectedYear: number;
    public transactionList = null;
    public testNumber: string;

    constructor(
        private paymentService: PaymentService,
        private modalService: NzModalService,
        private router: Router,
        private translate: TranslateService,
        private accountService: AccountService,
        private authService: AuthService) { }

    ngOnInit() {

        this.setDoughnutPlugins();
        this.setLastsYears();
        this.user = this.authService.getUser();
        this.applicationSubscription = this.authService.getApplicationSuscription();
        this.organizationId = this.applicationSubscription.w0791;
        this.getPaymentsInfo();
        this.getAccountCurrency(this.organizationId);
        this.getHistoricPaymentsInfo(this.organizationId);
    }

    public getPaymentsInfo() {
        const request = {
            organizationId: this.organizationId,
            lastMonth: this.lastMonth
        };
        this.paymentService.getPaymentsInfo(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.processTransactionInfo(instance.Data);
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getHistoricPaymentsInfo(appSubscriptionId: string) {
        const data = {
            organizationId: appSubscriptionId,
            userCode: '',
            year: this.selectedYear
        };
        this.paymentService.getHistoricPaymentsInfo(data).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.processHistoricTransactionInfo(instance.Data);
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getLastPayments(appSubscriptionId: string) {
        const data = {
            organizationId: appSubscriptionId,
            currency: this.selectedCurrency,
            batch: Constants.LAST_TRANSACTIONS,
            lastMonth: 0,
            pag: 0,
            regxpag: 0
        };
        this.paymentService.getTransactions(data).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    if (instance.Data) {
                        this.transactionList = instance.Data.QData;
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public processTransactionInfo(transactionInfo: any) {
        this.transactionInfo.transactionsPEN = transactionInfo.tranPEN;
        this.transactionInfo.transactionsUSD = transactionInfo.tranUSD;
        this.transactionInfo.amountPEN = transactionInfo.amountPEN;
        // 'Ochenta con 00/100 Soles';
        this.transactionInfo.amountUSD = transactionInfo.amountUSD;

        // 'Veinte con 00/100 Dólares';
        this.transactionInfo.transactionsTotal = transactionInfo.tranPEN + transactionInfo.tranUSD;
        this.transactionInfo.transactionsPercentPEN = transactionInfo.tranPENPercent;
        this.transactionInfo.transactionsPercentUSD = transactionInfo.tranUSDPercent;

        this.doughnutData.push(this.transactionInfo.transactionsPercentPEN);
        this.doughnutData.push(this.transactionInfo.transactionsPercentUSD);
        this.doughnutLabels.push(Constants.PEN_CODE);
        this.doughnutLabels.push(Constants.USD_CODE);
    }

    public processHistoricTransactionInfo(historicTransactionInfo: any) {
        this.barChartData[0].data = historicTransactionInfo.PEN ? historicTransactionInfo.PEN : this.barChartData[0].data;
        this.barChartData[1].data = historicTransactionInfo.USD ? historicTransactionInfo.USD : this.barChartData[1].data;
    }

    public setDoughnutPlugins() {
        const payments = this.translate.instant('ADMIN.HOME.LABEL.PAYMENTS');
        const forCurrency = this.translate.instant('ADMIN.HOME.LABEL.FOR_CURRENCY');
        this.doughnutChartPlugins =
            [{
                beforeDraw(chart) {
                    const width = chart.width;
                    const height = chart.height;
                    const ctx = chart.ctx;

                    ctx.restore();
                    const fontSize = (height / 130).toFixed(2);
                    ctx.font = fontSize + 'em Poppins-Light';
                    ctx.textBaseline = 'middle';

                    let text = '% ' + payments;
                    let textX = Math.round((width - ctx.measureText(text).width) / 2);
                    const textY = height / 2;
                    ctx.fillText(text, textX, textY - 10);

                    text = forCurrency;
                    textX = Math.round((width - ctx.measureText(text).width) / 2);
                    ctx.fillText(text, textX, textY + 10);
                    ctx.save();
                }
            }];
    }

    public setLastsYears() {
        let last = 5;
        let thisYear = new Date().getFullYear();
        this.years = new Array<number>();
        this.years.push(thisYear);
        this.selectedYear = thisYear;

        for (last; last > 1; last--) {
            thisYear = thisYear - 1;
            this.years.push(thisYear);
        }
    }

    public getAccountCurrency(appSubscriptionId: string) {
        this.accountService.getAccountCurrency(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data && instance.Data.Currency) {
                            this.accountCurrencyList = instance.Data.Currency;
                            this.selectedCurrency = this.accountCurrencyList[this.accountCurrencyList.length - 1];
                            this.getLastPayments(this.organizationId);
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            }
        );
    }

    public yearChange(event: Event) {
        this.getHistoricPaymentsInfo(this.organizationId);
    }

    public currencyChange(event: Event) {
        this.getLastPayments(this.organizationId);
    }

    public isLarger(): boolean {
        let larger = false;
        if ((this.transactionInfo.amountPEN && this.transactionInfo.amountPEN.length > 7) ||
            (this.transactionInfo.amountUSD && this.transactionInfo.amountUSD.length > 7)) {
            larger = true;
        }
        return larger;
    }

    // public getPpeAccount(appSubscriptionId: string) {
    //     this.accountService.getAccount(appSubscriptionId).subscribe(
    //         (instance: any) => {
    //             if (instance) {
    //                 if (instance.Result === 0) {
    //                     if (instance.Data) {
    //                         console.log(instance);
    //                         this.accountPpe = instance.Data;
    //                     } else {
    //                         // this.getOrganizationByUserId(this.user.uid);
    //                     }
    //                 } else {
    //                     this.modalService.warning({
    //                         nzTitle: this.translate.instant('MODAL.WARNING'),
    //                         nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
    //                         // nzContent: 'La operación se ha realizado exitosamente'
    //                     });
    //                 }

    //             }
    //         }
    //     );
    // }

    // getLastDays(days: number) {
    //     const labels = this.lineLabels.slice();
    //     this.lineChartLabels = labels.slice(-days);

    //     const data = [
    //         [65, 59, 80, 81, 56, 55, 40, 50, 60, 70],
    //         [28, 48, 40, 19, 86, 27, 90, 85, 95, 75]
    //     ];

    //     for (let i = 0; i < this.lineChartData.length; i++) {
    //         this.lineChartData[i].data = data[i].slice(-days);
    //     }
    // }

    // public register() {
    //     this.subscriptionService.getSubscriptionByUserId(this.user.uid).subscribe(
    //         (subscription: any) => {
    //             this.applicationSubscription = new ApplicationSubscription();
    //             this.applicationSubscription.b4321 = subscription.b4321;
    //             this.applicationSubscription.k0871 = this.applicationId;

    //             this.applicationBySubscriptionService.register(this.applicationSubscription).subscribe(
    //                 (instance: any) => {
    //                     if (instance.status) {
    //                         this.modalService.success({
    //                             nzTitle: this.translate.instant('MODAL.SUCCESS'),
    //                             nzContent: this.translate.instant('MODAL.SUCCESS_MESSAGE')
    //                             // nzContent: 'La operación se ha realizado exitosamente'
    //                         });
    //                         this.router.navigate(['/admin']);
    //                     } else {
    //                         this.modalService.warning({
    //                             nzTitle: this.translate.instant('MODAL.WARNING'),
    //                             nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
    //                             // nzContent: 'La operación se ha realizado exitosamente'
    //                         });
    //                     }
    //                 }
    //             );
    //         }
    //     );
    // }
}
