import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/core/auth.service';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { AccountPpe } from 'src/app/class/accountPpe';
import { AccountService } from 'src/app/adminservices/account.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { OrganizationService } from 'src/app/adminservices/organization.service';
import { AccountParameter } from 'src/app/class/accountParameter';
import { RegExpresion, SpecialKeys, Constants, MinLength, MaxLength } from 'src/app/class/constants';
import { UserService } from 'src/app/adminservices/user.service';
import { CustomValidators } from 'src/app/validators/custom-validators';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    MaxLength: any = MaxLength;
    public user: any;
    public applicationSubscription: ApplicationSubscription;
    public accountPpe = new AccountPpe();
    public organization: any;
    public bankList = [];
    public fbUserForm: FormGroup;
    public profileForm: FormGroup;
    public solesBankForm: FormGroup;
    public dolarBankForm: FormGroup;
    public passwordVisible = false;
    public accountParameters: Array<AccountParameter>;

    constructor(
        private translate: TranslateService,
        private authService: AuthService,
        private accountService: AccountService,
        private organizationService: OrganizationService,
        private modalService: NzModalService,
        private userService: UserService,
        private nzNotification: NzNotificationService,
        private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.createFbUserForm();
        this.createProfileForm();
        this.createSolesBankForm();
        this.createDolarBankForm();
        this.user = this.authService.getUser();
        console.log('USER => ', this.user);
        this.applicationSubscription = this.authService.getApplicationSuscription();
        this.accountPpe.organizationId = this.applicationSubscription.w0791;
        this.accountPpe.userCode = this.user.displayName;
        this.getPpeAccount(this.accountPpe.organizationId);
        this.getDocumentTypes();
        this.getBanks();
    }

    public createFbUserForm(){
        this.fbUserForm = this.formBuilder.group({
            password: [null, [Validators.required, Validators.minLength(MinLength.PASSWORD)]]
        });
    }
    public createProfileForm() {
        this.profileForm = this.formBuilder.group({
            userName: [null, [Validators.required]],
            documentType: [null, [Validators.required]],
            documentNumber: [null, [Validators.required]],
            email: [null, [Validators.required, Validators.email]],
            movil: [null, [Validators.required, CustomValidators.phoneNumber]]
        });
    }

    public createSolesBankForm() {
        this.solesBankForm = this.formBuilder.group({
            solesBank: [null],
            solesAccountNumber: [null],
            solesCci: [null]
        });
    }

    public createDolarBankForm() {
        this.dolarBankForm = this.formBuilder.group({
            dolarBank: [null],
            dolarAccountNumber: [null],
            dolarCci: [null]
        });
    }

    public getPpeAccount(appSubscriptionId: string) {
        this.accountService.getAccount(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        this.processData(instance.Data);
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                }
            }
        );
    }

    public processData(accountPpe: any) {
        this.accountPpe = accountPpe;
        this.profileForm.setValue({
            userName: this.accountPpe.userName,
            documentType: this.accountPpe.documentType,
            documentNumber: this.accountPpe.documentNumber,
            email: this.accountPpe.parameters.find(x => x.code === 'email') ?
                this.accountPpe.parameters.find(x => x.code === 'email').value : null,
            movil: this.accountPpe.parameters.find(x => x.code === 'movil') ?
                this.accountPpe.parameters.find(x => x.code === 'movil').value : null
        });

        this.solesBankForm.setValue({
            solesBank: this.accountPpe.parameters.find(x => x.code === 'solesBank') ?
                this.accountPpe.parameters.find(x => x.code === 'solesBank').value : null,

            solesAccountNumber: this.accountPpe.parameters.find(x => x.code === 'solesAccountNumber') ?
                this.accountPpe.parameters.find(x => x.code === 'solesAccountNumber').value : null,

            solesCci: this.accountPpe.parameters.find(x => x.code === 'solesCci') ?
                this.accountPpe.parameters.find(x => x.code === 'solesCci').value : null,
        });

        this.dolarBankForm.setValue({
            dolarBank: this.accountPpe.parameters.find(x => x.code === 'dolarBank') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarBank').value : null,

            dolarAccountNumber: this.accountPpe.parameters.find(x => x.code === 'dolarAccountNumber') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarAccountNumber').value : null,

            dolarCci: this.accountPpe.parameters.find(x => x.code === 'dolarCci') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarCci').value : null
        });

        this.updateDocumentValidator(this.documentType.value);

        this.solesBankForm.markAsDirty();
        this.dolarBankForm.markAsDirty();
        this.profileForm.markAsDirty();
        this.solesBankForm.updateValueAndValidity();
        this.dolarBankForm.updateValueAndValidity();
        this.profileForm.updateValueAndValidity();
    }

    // public getOrganizationByUserId(userId: string) {
    //     this.organizationService.getOrganizationByUserId(userId).subscribe(
    //         (instance: any) => {
    //             if (instance) {
    //                 this.organization = instance;
    //                 console.log(this.organization);
    //                 this.processOrganizationData();
    //             }
    //         }
    //     );
    // }

    // public processOrganizationData() {
    //     this.profileForm.setValue({
    //         userName: this.organization.f4472,
    //         documentType: null,
    //         documentNumber: this.organization.f4470,
    //         email: this.organization.f4471,
    //         movil: this.organization.f4469,
    //         password: null
    //     });

    //     this.solesBankForm.markAsDirty();
    //     this.dolarBankForm.markAsDirty();
    //     this.profileForm.markAsDirty();
    //     this.solesBankForm.updateValueAndValidity();
    //     this.dolarBankForm.updateValueAndValidity();
    //     this.profileForm.updateValueAndValidity();
    // }

    public getDocumentTypes() {
    }

    public getBanks() {
        this.accountService.getAccountBanks('PE').subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.bankList = instance.Data.QData;
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                }
            }
        );
    }

    public updateUserInfo() {
        this.accountPpe.userName = this.userName.value;
        this.accountPpe.documentType = this.documentType.value;
        this.accountPpe.documentNumber = this.documentNumber.value;
        this.accountParameters = new Array<AccountParameter>();
        this.accountAddParameter(this.accountParameters, 'email', this.email.value);
        this.accountAddParameter(this.accountParameters, 'movil', this.movil.value);
        this.accountPpe.parameters = this.accountParameters;

        this.accountService.updateUserInfo(this.accountPpe).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        this.nzNotification.create(
                            'success',
                            this.translate.instant('MODAL.SUCCESS'),
                            this.translate.instant('ADMIN.PROFILE.NOTIFICATION_MESSAGE.SUCCESS_UPDATE'),
                        );
                        this.getPpeAccount(this.accountPpe.organizationId);
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            }
        );
    }

    public updateBankInfo() {
        this.accountParameters = new Array<AccountParameter>();
        this.accountAddParameter(this.accountParameters, 'solesBank',
            this.solesAccountNumber.value && this.solesCci.value ? this.solesBank.value : null);
        this.accountAddParameter(this.accountParameters, 'solesAccountNumber', this.solesAccountNumber.value);
        this.accountAddParameter(this.accountParameters, 'solesCci', this.solesCci.value);
        this.accountAddParameter(this.accountParameters, 'dolarBank',
            this.dolarAccountNumber.value && this.dolarCci.value ? this.dolarBank.value : null);
        this.accountAddParameter(this.accountParameters, 'dolarAccountNumber', this.dolarAccountNumber.value);
        this.accountAddParameter(this.accountParameters, 'dolarCci', this.dolarCci.value);
        this.accountPpe.parameters = this.accountParameters;

        this.accountService.updateBankInfo(this.accountPpe).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        this.nzNotification.create(
                            'success',
                            this.translate.instant('MODAL.SUCCESS'),
                            this.translate.instant('ADMIN.PROFILE.NOTIFICATION_MESSAGE.SUCCESS_UPDATE'),
                        );
                        this.getPpeAccount(this.accountPpe.organizationId);
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            }
        );
    }

    public updatePassword() {
        this.userService.userUpdatePass(this.user.uid, this.password.value).subscribe(
            (instance: any) => {
                if (instance.status) {
                    this.nzNotification.create(
                        'success',
                        this.translate.instant('MODAL.SUCCESS'),
                        this.translate.instant('ADMIN.PROFILE.NOTIFICATION_MESSAGE.SUCCESS_PASS_UPDATE'),
                    );
                    this.fbUserForm.setValue({
                        password : null
                    });
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    // public saveUserInfo() {
    //     this.accountPpe.userName = this.userName.value;
    //     this.accountPpe.documentType = this.documentType.value;
    //     this.accountPpe.documentNumber = this.documentNumber.value;
    //     this.accountPpe.businessName = this.organization.f4462;
    //     this.accountPpe.businessAddress = this.organization.f4463 + ' ' + this.organization.f4464;

    //     this.accountParameters = new Array<AccountParameter>();

    //     this.accountAddParameter(this.accountParameters, 'email', this.email.value);
    //     this.accountAddParameter(this.accountParameters, 'movil', this.movil.value);
    //     this.accountAddParameter(this.accountParameters, 'solesBank', this.solesBank.value);
    //     this.accountAddParameter(this.accountParameters, 'solesAccountNumber', this.solesAccountNumber.value);
    //     this.accountAddParameter(this.accountParameters, 'solesCci', this.solesCci.value);
    //     this.accountAddParameter(this.accountParameters, 'dolarBank', this.dolarBank.value);
    //     this.accountAddParameter(this.accountParameters, 'dolarAccountNumber', this.dolarAccountNumber.value);
    //     this.accountAddParameter(this.accountParameters, 'dolarCci', this.dolarCci.value);
    //     this.accountPpe.parameters = this.accountParameters;

    //     this.accountService.saveAccount(this.accountPpe).subscribe(
    //         (instance: any) => {
    //             if (instance) {
    //                 console.log(instance);
    //                 if (instance.Result === 0) {
    //                     this.nzNotification.create(
    //                         'success',
    //                         this.translate.instant('MODAL.SUCCESS'),
    //                         this.translate.instant('ADMIN.PROFILE.NOTIFICATION_MESSAGE.SUCCESS_SAVE'),
    //                     );
    //                 } else {
    //                     this.modalService.warning({
    //                         nzTitle: this.translate.instant('MODAL.WARNING'),
    //                         nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
    //                     });
    //                 }
    //             }
    //         }
    //     );
    // }

    public accountAddParameter(parameters: Array<AccountParameter>, code: string, value: string) {
        if (value) {
            const parameter = new AccountParameter();
            parameter.code = code;
            parameter.value = value;
            parameters.push(parameter);
        }
    }

    public updateDocumentValidator(event: any) {

        switch (Number(event)) {
            case 1:
                this.documentNumber.setValidators([Validators.required, dniDocument()]);
                break;
            case 2:
                this.documentNumber.setValidators([Validators.required, rucDocument()]);
                break;
            case 3:
            case 4:
                break;
            default:
                break;
        }
        this.documentNumber.markAsDirty();
        this.documentNumber.updateValueAndValidity();
    }

    public updateSolesBankValidator(event: Event) {
        if (this.solesAccountNumber.value || this.solesCci.value) {
            this.solesBank.setValidators([Validators.required]);
            this.solesAccountNumber.setValidators([Validators.required, accountNumber()]);
            this.solesCci.setValidators([Validators.required, accountNumber()]);
        } else {
            this.solesBank.setValidators(null);
            this.solesAccountNumber.setValidators(null);
            this.solesCci.setValidators(null);
        }
        this.solesBank.markAsDirty();
        this.solesAccountNumber.markAsDirty();
        this.solesCci.markAsDirty();
        this.solesBank.updateValueAndValidity();
        this.solesAccountNumber.updateValueAndValidity();
        this.solesCci.updateValueAndValidity();
    }

    public updateDolarBankValidator(event: Event) {
        if (this.dolarAccountNumber.value || this.dolarCci.value) {
            this.dolarBank.setValidators([Validators.required]);
            this.dolarAccountNumber.setValidators([Validators.required, accountNumber()]);
            this.dolarCci.setValidators([Validators.required, accountNumber()]);
        } else {
            this.dolarBank.setValidators(null);
            this.dolarAccountNumber.setValidators(null);
            this.dolarCci.setValidators(null);
        }
        this.dolarBank.markAsDirty();
        this.dolarAccountNumber.markAsDirty();
        this.dolarCci.markAsDirty();
        this.dolarBank.updateValueAndValidity();
        this.dolarAccountNumber.updateValueAndValidity();
        this.dolarCci.updateValueAndValidity();
    }    

    uNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_USERNAME;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.USERNAME_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    mNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_MAIL;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.MAIL_MAX_LENGTH)) {
            event.preventDefault();
        }
    }      

    idOnlyNumber(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_NUMBER;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.ID_COMPANY_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    get password() {
        return this.fbUserForm.get('password');
    }
    get userName() {
        return this.profileForm.get('userName');
    }
    get documentType() {
        return this.profileForm.get('documentType');
    }
    get documentNumber() {
        return this.profileForm.get('documentNumber');
    }
    get email() {
        return this.profileForm.get('email');
    }
    get movil() {
        return this.profileForm.get('movil');
    }    
    get solesBank() {
        return this.solesBankForm.get('solesBank');
    }
    get solesAccountNumber() {
        return this.solesBankForm.get('solesAccountNumber');
    }
    get solesCci() {
        return this.solesBankForm.get('solesCci');
    }
    get dolarBank() {
        return this.dolarBankForm.get('dolarBank');
    }
    get dolarAccountNumber() {
        return this.dolarBankForm.get('dolarAccountNumber');
    }
    get dolarCci() {
        return this.dolarBankForm.get('dolarCci');
    }
}

function dniDocument(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{8}$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { dniDocument: true };
    };
}

function rucDocument(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{11}$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { rucDocument: true };
    };
}

function accountNumber(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{14,}$/;
        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { accountNumber: true };
    };
}