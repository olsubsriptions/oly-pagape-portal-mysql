import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CustomDirectivesModule } from 'src/app/directives/custom-directives.module';


@NgModule({
    declarations: [ProfileComponent],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        NgZorroAntdModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        CustomDirectivesModule
    ]
})
export class ProfileModule { }
