import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { Pagination } from 'src/app/class/pagination';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { PaymentLink } from 'src/app/class/paymentLink';
import { PaymentlinkService } from 'src/app/adminservices/paymentlink.service';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { Status, ExternalLink } from 'src/app/class/enums';
import { Constants } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';
import { AccountService } from 'src/app/adminservices/account.service';
import { PaymentLinksStats } from 'src/app/class/paymentLinksStats';
import { UtilService } from 'src/app/adminservices/util.service';

@Component({
    selector: 'app-links',
    templateUrl: './links.component.html',
    styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {

    public user: any;
    public unique = Constants.UNIQUE_PAYMENTLINK;
    public multiple = Constants.MULTIPLE_PAYMENTLINK;
    public applicationSuscription: ApplicationSubscription;
    public paymentLink = new PaymentLink();
    public pagapeShortName = Constants.pagapeCode;
    public regxpag = Constants.regxpag;
    public paymentLinkList = [];
    public status = Status;
    public externalLink = ExternalLink;
    public qrModalLink: string;
    public modal: NzModalRef;
    public modalData: any;
    public confirmModal: NzModalRef;
    public accountCurrencyList = new Array<string>();
    public paymentLinkType: number;
    public organizationId: string;
    public paginationUnique: Pagination;
    public paginationMultiple: Pagination;
    public uniqueLastMonth = 0;
    public multipleLastMonth = 0;
    public paymentLinkUniqueList = null;
    public paymentLinkMultipleList = null;
    public selectedUniqueCurrency;
    public selectedMultipleCurrency;
    public paymentLinksStats = new PaymentLinksStats();

    constructor(
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private paymentlinkService: PaymentlinkService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private accountService: AccountService,
        private authService: AuthService,
        private utilService: UtilService
    ) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.paginationUnique = new Pagination(1, this.regxpag, 0);
        this.paginationMultiple = new Pagination(1, this.regxpag, 0);
        this.applicationSuscription = this.authService.getApplicationSuscription();
        this.organizationId = this.applicationSuscription.w0791;
        this.getAccountCurrency(this.organizationId);
        this.getPaymentLinksStats(this.organizationId);
    }

    public getAccountCurrency(appSubscriptionId: string) {
        this.accountService.getAccountCurrency(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data && instance.Data.Currency) {
                            this.accountCurrencyList = instance.Data.Currency;
                            this.selectedUniqueCurrency = this.accountCurrencyList[this.accountCurrencyList.length - 1];
                            this.selectedMultipleCurrency = this.accountCurrencyList[this.accountCurrencyList.length - 1];
                            // this.selectedUniqueCurrency = this.accountCurrencyList[0];
                            // this.selectedMultipleCurrency = this.accountCurrencyList[0];
                            this.getUniquePaymentLinks(this.organizationId);
                            this.getMultiplePaymentLinks(this.organizationId);
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                }
            }
        );
    }

    public getPaymentLinksStats(appSubscriptionId: string) {
        this.paymentlinkService.getPaymentLinksStats(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    // this.paymentLinksStats = this.formatNumbers(instance.Data);
                    this.paymentLinksStats = instance.Data;
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getUniquePaymentLinks(appSubscriptionId: string) {
        const request = {
            organizationId: appSubscriptionId,
            type: Constants.UNIQUE_PAYMENTLINK,
            currency: this.selectedUniqueCurrency,
            lastMonth: this.uniqueLastMonth,
            regxpag: this.paginationUnique.regxpag,
            pag: this.paginationUnique.pag
        };

        this.paymentlinkService.getPaymentLinks(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.paymentLinkUniqueList = instance.Data.QData;
                    this.paginationUnique.totalrows = instance.Data.RowCount;

                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public getMultiplePaymentLinks(appSubscriptionId: string) {
        const request = {
            organizationId: appSubscriptionId,
            type: Constants.MULTIPLE_PAYMENTLINK,
            currency: this.selectedMultipleCurrency,
            lastMonth: this.multipleLastMonth,
            regxpag: this.paginationMultiple.regxpag,
            pag: this.paginationMultiple.pag
        };

        this.paymentlinkService.getPaymentLinks(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.paymentLinkMultipleList = instance.Data.QData;
                    this.paginationMultiple.totalrows = instance.Data.RowCount;

                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }


    public processPaymentLinkListData(data: any) {
        this.paymentLinkList = data;
        if (this.paymentLinkList) {
            this.paymentLinkList.forEach(paymentLink => {
                paymentLink.linkData = JSON.parse(paymentLink.linkData);
                paymentLink.linkData.shortComment = paymentLink.linkData.detail;

                paymentLink.status = Number(paymentLink.status);
            });
        }
    }

    public showQr(paymentLink: any, qrModalTemplate: TemplateRef<{}>) {
        this.qrModalLink = this.externalLink.PAGAPE + paymentLink.link;
        this.modal = this.modalService.create({
            nzContent: qrModalTemplate,
            nzFooter: null
        });
    }

    public getQrWidth() {
        return this.utilService.getQrWidth();
    }

    public showPaymentLink(paymentlink: any, linkInfoModalTemplate: TemplateRef<{}>) {
        this.modalData = paymentlink;
        this.modal = this.modalService.create({
            nzTitle: this.externalLink.PAGAPE + this.modalData.link,
            nzWidth: '800px',
            nzContent: linkInfoModalTemplate,
            nzFooter: null
        });
    }

    public paginationUniqueChange(event: Event) {
        this.paginationUnique.pag = Number(event);
        this.getUniquePaymentLinks(this.organizationId);
    }

    public paginationMultipleChange(event: Event) {
        this.paginationMultiple.pag = Number(event);
        this.getMultiplePaymentLinks(this.organizationId);
    }

    public uniqueCurrencyChange(event: Event) {
        this.getUniquePaymentLinks(this.organizationId);
    }

    public multipleCurrencyChange(event: Event) {
        this.getMultiplePaymentLinks(this.organizationId);
    }

    public getUnique(flagLastMonth: number) {
        if (this.uniqueLastMonth !== flagLastMonth) {
            this.uniqueLastMonth = flagLastMonth;
            this.getUniquePaymentLinks(this.organizationId);
        }
    }

    public getMultiple(flagLastMonth: number) {
        if (this.multipleLastMonth !== flagLastMonth) {
            this.multipleLastMonth = flagLastMonth;
            this.getMultiplePaymentLinks(this.organizationId);
        }

    }

    // public changeStatus(paymentlink: any) {
    //     this.confirmModal = this.modalService.confirm({
    //         nzTitle: paymentlink.status === 0 ?
    //             this.translate.instant('ADMIN.PAYMENT_LINK.ENABLE_PAYMENT_LINK') :
    //             this.translate.instant('ADMIN.PAYMENT_LINK.DISABLE_PAYMENT_LINK'),
    //         nzOkText: this.translate.instant('ADMIN.MODAL_UPDATE_TEXT'),
    //         nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
    //         nzContent: paymentlink.status === 0 ?
    //             this.translate.instant('ADMIN.PAYMENT_LINK.MODAL_ENABLE_PAYMENT_LINK') :
    //             this.translate.instant('ADMIN.PAYMENT_LINK.MODAL_DISABLE_PAYMENT_LINK'),
    //         nzOnOk: () => {
    //             this.changeStatusPaymentLink(paymentlink);
    //         }
    //     });
    // }

    // public showDelete(paymentlink: any) {
    //     this.confirmModal = this.modalService.confirm({
    //         nzTitle: this.translate.instant('ADMIN.PAYMENT_LINK.DELETE_PAYMENT_LINK'),
    //         nzOkText: this.translate.instant('ADMIN.MODAL_DELETE_TEXT'),
    //         nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
    //         nzContent: this.translate.instant('ADMIN.PAYMENT_LINK.MODAL_DELETE_PAYMENT_LINK'),
    //         nzOnOk: () => {
    //             this.deletePaymentLink(paymentlink);
    //         }
    //     });
    // }

    public changeStatusPaymentLink(paymentLink: any, linkType: number) {
        const request = {
            organizationId: paymentLink.orgid,
            link: paymentLink.link,
            status: paymentLink.enable ? 0 : 1
        };
        this.paymentlinkService.changeStatusPaymentLink(request).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    switch (linkType) {
                        case Constants.UNIQUE_PAYMENTLINK: {
                            this.getUniquePaymentLinks(this.organizationId);
                            break;
                        }
                        case Constants.MULTIPLE_PAYMENTLINK: {
                            this.getMultiplePaymentLinks(this.organizationId);
                            break;
                        }
                        default: break;
                    }
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    // public deletePaymentLink(paymentLink: PaymentLink) {
    //     this.paymentlinkService.deletePaymentLink(paymentLink).subscribe(
    //         (instance: any) => {
    //             if (instance.Result === 0) {
    //                 this.getPaymentLinks(this.paymentLink.organizationId, this.pagination);
    //                 this.modalService.success({
    //                     nzTitle: this.translate.instant('MODAL.SUCCESS'),
    //                     nzContent: this.translate.instant('MODAL.SUCCESS_MESSAGE')
    //                 });
    //             } else {
    //                 this.modalService.warning({
    //                     nzTitle: this.translate.instant('MODAL.WARNING'),
    //                     nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
    //                 });
    //             }
    //         }
    //     );
    // }
}
