import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinksComponent } from './links/links.component';
import { PaymentlinkmenuComponent } from './paymentlinkmenu/paymentlinkmenu.component';
import { PaymentlinkComponent } from './paymentlink/paymentlink.component';
import { MultipleComponent } from './multiple/multiple.component';
import { UniqueComponent } from './unique/unique.component';
import { AuthGuard } from 'src/app/core/auth.guard';


const routes: Routes = [
  {
    path: 'generate',
    component: PaymentlinkmenuComponent
  },
  // {
  //   path: 'generate/:1',
  //   component: PaymentlinkComponent
  // },
  {
    path: 'generate/unique',
    component: UniqueComponent
  },
  {
    path: 'generate/multiple',
    component: MultipleComponent
  },
  {
    path: 'links',
    component: LinksComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentlinkRoutingModule { }
