import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentlinkmenuComponent } from './paymentlinkmenu.component';

describe('PaymentlinkmenuComponent', () => {
  let component: PaymentlinkmenuComponent;
  let fixture: ComponentFixture<PaymentlinkmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentlinkmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentlinkmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
