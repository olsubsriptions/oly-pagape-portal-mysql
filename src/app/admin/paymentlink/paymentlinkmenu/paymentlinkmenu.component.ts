import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-paymentlinkmenu',
  templateUrl: './paymentlinkmenu.component.html',
  styleUrls: ['./paymentlinkmenu.component.scss']
})
export class PaymentlinkmenuComponent implements OnInit {

  public coordsButton1 = '0,0,0,0';
  public coordsButton2 = '0,0,0,0';
  public element: HTMLElement;

  constructor() { }

  ngOnInit() {
  }

  @HostListener('window:resize', ['$event'])
  getCoords(event) {
    this.element = document.getElementById('mydiv') as HTMLElement;
    const width = this.element.offsetWidth;
    const heigth = this.element.offsetHeight;
    const a = Math.round(width * 59.3 / 100).toString();
    const b = Math.round(heigth * 13 / 100).toString();
    const c = Math.round(width * 80.7 / 100).toString();
    const d = Math.round(heigth * 25.9 / 100).toString();
    this.coordsButton1 = a + ',' + b + ',' + c + ',' + d;
    const f = Math.round(heigth * 42.9 / 100).toString();
    const h = Math.round(heigth * 56.2 / 100).toString();
    this.coordsButton2 = a + ',' + f + ',' + c + ',' + h;
  }
}
