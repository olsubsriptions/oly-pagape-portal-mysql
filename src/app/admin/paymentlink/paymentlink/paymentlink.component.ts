import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { NzModalRef, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { PaymentlinkService } from 'src/app/adminservices/paymentlink.service';
import * as uuid from 'uuid';
import { PaymentLink } from 'src/app/class/paymentLink';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { LinkData } from 'src/app/class/linkData';
import { ExternalLink } from 'src/app/class/enums';
import { Constants, RegExpresion, SpecialKeys } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';
import { AccountService } from 'src/app/adminservices/account.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as crc32 from 'crc-32';

@Component({
    selector: 'app-paymentlink',
    templateUrl: './paymentlink.component.html',
    styleUrls: ['./paymentlink.component.scss']
})
export class PaymentlinkComponent implements OnInit {
    public buttonTest = 3;
    public user: any;
    public saveLoading = false;
    public qrLink: string;
    public link: string;
    public modal: NzModalRef;
    public code: string;
    public paymentlinkForm: FormGroup;
    public paymentLink = new PaymentLink();
    public paymentLinkType: number;
    public addAmount = false;
    public externalLink = ExternalLink;
    public applicationSuscription: ApplicationSubscription;
    public constants = Constants;
    public pagapeShortName = Constants.pagapeCode;
    public accountCurrencyList = new Array<string>();

    constructor(
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        private paymentlinkService: PaymentlinkService,
        private accountService: AccountService,
        private nzNotification: NzNotificationService,
        private modalService: NzModalService,
        private authService: AuthService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.paymentLinkType = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.createPaymentlinkForm();
        if (this.paymentLinkType === Constants.UNIQUE_PAYMENTLINK || this.paymentLinkType === Constants.MULTIPLE_PAYMENTLINK) {
            this.user = this.authService.getUser();

            if (this.paymentLinkType === Constants.MULTIPLE_PAYMENTLINK) {
                this.amount.disable();
                this.currency.disable();
                this.paymentlinkForm.updateValueAndValidity();
            }
            this.applicationSuscription = this.authService.getApplicationSuscription();
            if (this.applicationSuscription) {
                this.paymentLink.organizationId = this.applicationSuscription.w0791;
                this.paymentLink.userCode = this.user.displayName;
                this.getAccountCurrency(this.paymentLink.organizationId);
            }

            // this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
            //     (instance: any) => {
            //         if (instance) {
            //             this.applicationSuscription = instance.find(x => x.w0799 === this.pagapeShortName);
            //             if (this.applicationSuscription) {
            //                 this.paymentLink.organizationId = this.applicationSuscription.w0791;
            //                 this.paymentLink.userCode = this.user.displayName;
            //                 // this.getAccountDefaultConfig(this.paymentLink.organizationId);
            //                 this.getAccountCurrency(this.paymentLink.organizationId);
            //             } else {
            //             }
            //         } else {
            //         }
            //     }
            // );
        } else {
            this.router.navigate(['/admin/paymentlink/generate']);
        }
    }

    public createPaymentlinkForm() {
        this.paymentlinkForm = this.formBuilder.group({
            detail: [null, [Validators.maxLength(50)]],
            currency: [null, [Validators.required]],
            amount: [null, [Validators.required, currencyAmount()]]
        });
    }

    // public getAccountDefaultConfig(appSubscriptionId: string) {
    //     this.accountService.getAccountDefaultConfig(appSubscriptionId).subscribe(
    //         (instance: any) => {
    //             if (instance) {
    //                 if (instance.Result === 0) {
    //                     if (instance.Data) {
    //                         this.processData(instance.Data.defaultConfig);
    //                     }
    //                 }
    //             }
    //         }
    //     );
    // }

    // public processData(defaultConfig: any) {

    //     const defaultConfigObj = JSON.parse(defaultConfig);
    //     console.log(defaultConfigObj);

    //     this.paymentlinkForm.setValue({
    //         detail: defaultConfigObj.detail,
    //         currency: defaultConfigObj.currency,
    //         amount: defaultConfigObj.amount,
    //         singlePayment: defaultConfigObj.singlePayment,
    //         expiration: defaultConfigObj.expiration,
    //         expirationDate: null,
    //         expirationTime: null,
    //         singleClient: defaultConfigObj.singleClient,
    //         notification: defaultConfigObj.notification,
    //         notificationType: defaultConfigObj.notificationType,
    //     });
    //     this.paymentlinkForm.updateValueAndValidity();
    // }

    public getAccountCurrency(appSubscriptionId: string) {
        this.accountService.getAccountCurrency(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data && instance.Data.Currency) {
                            this.accountCurrencyList = instance.Data.Currency;
                            if (this.paymentLinkType === Constants.UNIQUE_PAYMENTLINK) {
                                this.paymentlinkForm.patchValue({
                                    currency: this.accountCurrencyList[this.accountCurrencyList.length - 1]
                                });
                                this.paymentlinkForm.updateValueAndValidity();
                            }
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                }
            }
        );
    }

    public savePaymentLink() {
        this.saveLoading = true;
        const linkData = new LinkData();
        linkData.detail = this.detail.value;
        linkData.currency = this.currency.value;
        linkData.amount = this.amount.value;

        this.paymentLink.linkData = JSON.stringify(linkData);
        this.paymentLink.link = this.generateLink();
        this.paymentLink.linkType = this.paymentLinkType;
        this.paymentLink.expirationDate = '';

        this.paymentlinkService.savePaymentLink(this.paymentLink).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.link = this.externalLink.PAGAPE + this.paymentLink.link;
                    this.qrLink = this.externalLink.PAGAPE + this.paymentLink.link;
                    this.nzNotification.create(
                        'success',
                        this.translate.instant('MODAL.SUCCESS'),
                        this.translate.instant('ADMIN.PAYMENT_LINK.NOTIFICATION_MESSAGE.SUCCESS_SAVE'),
                    );
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
                this.saveLoading = false;
            },
            (error: any) => {
                this.saveLoading = false;
            }
        );
    }

    public generateLink(): string {
        const uid = uuid.v4();
        const generatedLink = crc32.str(uid).toString(16).replace('-', '') + this.getRandomFragment(uid);
        return generatedLink;
    }

    public getRandomFragment(uid: string): string {
        const splitedUid = uid.split('-');
        const randomIndex = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
        const fragment = splitedUid[randomIndex];
        return fragment;
    }

    // public showConfirm(): void {
    //     this.saveLoading = true;
    //     this.confirmModal = this.modalService.confirm({
    //         nzTitle: this.translate.instant('ADMIN.PAYMENT_LINK.GENERATE_PAYMENT_LINK'),
    //         nzContent: this.translate.instant('ADMIN.PAYMENT_LINK.MODAL_CREATE_PAYMENT_LINK'),
    //         nzOkText: this.translate.instant('ADMIN.MODAL_GENERATE_TEXT'),
    //         nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
    //         nzOnOk: () => {
    //             this.savePaymentLink();
    //         },
    //         nzOnCancel: () => {
    //             this.saveLoading = false;
    //         }
    //     });
    // }

    // public getDateFromForm() {
    //     const stringDate = this.expirationDate.value.getFullYear()
    //         + '-' + (this.padLeft(this.expirationDate.value.getMonth() + 1, '0', 2))
    //         + '-' + (this.padLeft(this.expirationDate.value.getDate(), '0', 2))
    //         + ' ' + (this.padLeft(this.expirationTime.value.getHours(), '0', 2))
    //         + ':' + (this.padLeft(this.expirationTime.value.getMinutes(), '0', 2))
    //         + ':00';
    //     return stringDate;
    // }
    // public padLeft(text: string, padChar: string, size: number): string {
    //     return (String(padChar).repeat(size) + text).substr((size * -1), size);
    // }

    public currencyNumber(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.PAYMENT_LINK_AMOUNT;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.AMOUNT_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    public noSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.PAYMENT_LINK_DETAIL;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;
        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.DETAIL_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    public copyLink(inputElement) {
        inputElement.select();
        document.execCommand('copy');
        inputElement.setSelectionRange(0, 0);
        this.nzNotification.create(
            'success',
            this.translate.instant('MODAL.SUCCESS'),
            this.translate.instant('MODAL.SUCCESS_MESSAGE'),
        );
    }

    public amountVisible(): boolean {
        return this.paymentLinkType === Constants.UNIQUE_PAYMENTLINK || this.addAmount;
    }

    public showAmount() {
        this.addAmount = !this.addAmount;
        if (this.addAmount) {
            this.paymentlinkForm.patchValue({
                currency: this.accountCurrencyList[0]
            });
            this.amount.enable();
            this.currency.enable();
        } else {
            this.paymentlinkForm.patchValue({
                amount: null,
                currency: null
            });
            this.amount.disable();
            this.currency.disable();
        }
        this.paymentlinkForm.updateValueAndValidity();
    }

    public newPaymentLink() {
        this.paymentlinkForm.reset();
        this.addAmount = false;
        this.link = null;
        this.qrLink = null;
        this.code = null;
        if (this.paymentLinkType === Constants.UNIQUE_PAYMENTLINK) {
            this.paymentlinkForm.patchValue({
                currency: this.accountCurrencyList[0]
            });
        } else if (this.paymentLinkType === Constants.MULTIPLE_PAYMENTLINK) {
            this.amount.disable();
            this.currency.disable();
        }
        this.paymentlinkForm.updateValueAndValidity();
    }

    public showQr(qrModalTemplate: TemplateRef<{}>) {
        if (this.qrLink) {
            this.modal = this.modalService.create({
                nzContent: qrModalTemplate,
                nzFooter: null
            });
        }
    }

    get detail() {
        return this.paymentlinkForm.get('detail');
    }
    get currency() {
        return this.paymentlinkForm.get('currency');
    }
    get amount() {
        return this.paymentlinkForm.get('amount');
    }


    // test button
    public buttonChange(num: number) {
        const options = [1, 2, 3];
        this.buttonTest = options[num % 3];
    }
}

function currencyAmount(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d+\.(\d{2})+$/;
        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { currencyAmount: true };
    };
}
