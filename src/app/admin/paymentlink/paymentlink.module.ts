import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentlinkRoutingModule } from './paymentlink-routing.module';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { PaymentlinkComponent } from './paymentlink/paymentlink.component';
import { QRCodeModule } from 'angularx-qrcode';
import { LinksComponent } from './links/links.component';
import { PaymentlinkmenuComponent } from './paymentlinkmenu/paymentlinkmenu.component';
import { MultipleComponent } from './multiple/multiple.component';
import { UniqueComponent } from './unique/unique.component';
import { CustomPipesModule } from 'src/app/pipes/custom-pipes.module';
import { CustomDirectivesModule } from 'src/app/directives/custom-directives.module';

@NgModule({
  declarations: [PaymentlinkComponent, LinksComponent, PaymentlinkmenuComponent, MultipleComponent, UniqueComponent],
  imports: [
    CommonModule,
    PaymentlinkRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    QRCodeModule,
    CustomPipesModule,
    CustomDirectivesModule
  ]
})
export class PaymentlinkModule { }
