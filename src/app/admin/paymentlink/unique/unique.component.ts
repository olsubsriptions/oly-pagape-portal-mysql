import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalRef, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { PaymentlinkService } from 'src/app/adminservices/paymentlink.service';
import * as uuid from 'uuid';
import { PaymentLink } from 'src/app/class/paymentLink';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { LinkData } from 'src/app/class/linkData';
import { ExternalLink } from 'src/app/class/enums';
import { Constants, MaxLength } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';
import { AccountService } from 'src/app/adminservices/account.service';
import * as crc32 from 'crc-32';
import { UtilService } from 'src/app/adminservices/util.service';
import { CustomValidators } from 'src/app/validators/custom-validators';

@Component({
    selector: 'app-unique',
    templateUrl: './unique.component.html',
    styleUrls: ['./unique.component.scss']
})
export class UniqueComponent implements OnInit {

    MaxLength: any = MaxLength;
    public user: any;
    public saveLoading = false;
    public qrLink: string;
    public link: string;
    public modal: NzModalRef;
    public code: string;
    public paymentlinkForm: FormGroup;
    public paymentLink = new PaymentLink();
    public paymentLinkType = Constants.UNIQUE_PAYMENTLINK;
    public externalLink = ExternalLink;
    public applicationSuscription: ApplicationSubscription;
    public constants = Constants;
    public pagapeShortName = Constants.pagapeCode;
    public accountCurrencyList = new Array<string>();

    constructor(
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        private paymentlinkService: PaymentlinkService,
        private accountService: AccountService,
        private nzNotification: NzNotificationService,
        private modalService: NzModalService,
        private utilService: UtilService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.createPaymentlinkForm();
        this.user = this.authService.getUser();

        this.applicationSuscription = this.authService.getApplicationSuscription();
        if (this.applicationSuscription) {
            this.paymentLink.organizationId = this.applicationSuscription.w0791;
            this.paymentLink.userCode = this.user.displayName;
            this.getAccountCurrency(this.paymentLink.organizationId);
        }
    }

    public createPaymentlinkForm() {
        this.paymentlinkForm = this.formBuilder.group({
            detail: [null, [Validators.required, Validators.maxLength(50)]],
            currency: [null, [Validators.required]],
            amount: [null, [Validators.required, CustomValidators.currencyAmount]]
        });
    }

    public getAccountCurrency(appSubscriptionId: string) {
        this.accountService.getAccountCurrency(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data && instance.Data.Currency) {
                            this.accountCurrencyList = instance.Data.Currency;
                            if (this.paymentLinkType === Constants.UNIQUE_PAYMENTLINK) {
                                this.paymentlinkForm.patchValue({
                                    currency: this.accountCurrencyList[this.accountCurrencyList.length - 1]
                                });
                                this.paymentlinkForm.updateValueAndValidity();
                            }
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                }
            }
        );
    }

    public savePaymentLink() {
        this.saveLoading = true;
        const linkData = new LinkData();
        linkData.detail = this.detail.value;
        linkData.currency = this.currency.value;
        linkData.amount = parseFloat(this.amount.value).toFixed(2);
        linkData.freePay = false;

        this.paymentLink.linkData = JSON.stringify(linkData);
        this.paymentLink.link = this.generateLink();
        this.paymentLink.linkType = this.paymentLinkType;
        this.paymentLink.expirationDate = '';

        this.paymentlinkService.savePaymentLink(this.paymentLink).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.link = this.externalLink.PAGAPE + this.paymentLink.link;
                    this.qrLink = this.externalLink.PAGAPE + this.paymentLink.link;
                    this.code = instance.Data.code;
                    this.nzNotification.create(
                        'success',
                        this.translate.instant('MODAL.SUCCESS'),
                        this.translate.instant('ADMIN.PAYMENT_LINK.NOTIFICATION_MESSAGE.SUCCESS_SAVE'),
                    );
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
                this.saveLoading = false;
            },
            (error: any) => {
                this.saveLoading = false;
            }
        );
    }

    public generateLink(): string {
        const uid = uuid.v4();
        const generatedLink = crc32.str(uid).toString(16).replace('-', '') + this.getRandomFragment(uid);
        return generatedLink;
    }

    public getRandomFragment(uid: string): string {
        const splitedUid = uid.split('-');
        const randomIndex = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
        const fragment = splitedUid[randomIndex];
        return fragment;
    }

    public copyLink(inputElement) {
        inputElement.select();
        document.execCommand('copy');
        inputElement.setSelectionRange(0, 0);
        this.nzNotification.create(
            'success',
            this.translate.instant('MODAL.SUCCESS'),
            this.translate.instant('MODAL.SUCCESS_MESSAGE'),
        );
    }

    public newPaymentLink() {
        if (this.qrLink) {
            this.paymentlinkForm.reset();
            this.link = null;
            this.qrLink = null;
            this.code = null;
            this.paymentlinkForm.patchValue({
                currency: this.accountCurrencyList[this.accountCurrencyList.length - 1]
            });

            this.paymentlinkForm.updateValueAndValidity();
        }
    }

    public showQr(qrModalTemplate: TemplateRef<{}>) {
        if (this.qrLink) {
            this.modal = this.modalService.create({
                nzContent: qrModalTemplate,
                nzFooter: null
            });
        }
    }

    public getQrWidth() {
        return this.utilService.getQrWidth();
    }

    public shareWhatsapp() {
        if (this.qrLink) {
            this.utilService.shareWhatsapp(this.link);
        }
    }

    public shareFacebook() {
        if (this.qrLink) {
            this.utilService.shareFacebook(this.link);
        }
    }

    public shareTwitter() {
        if (this.qrLink) {
            this.utilService.shareTwitter(this.link);
        }
    }

    get detail() {
        return this.paymentlinkForm.get('detail');
    }
    get currency() {
        return this.paymentlinkForm.get('currency');
    }
    get amount() {
        return this.paymentlinkForm.get('amount');
    }
}

