import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { ExternalLink } from 'src/app/class/enums';
import { AccountService } from 'src/app/adminservices/account.service';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { NzModalRef, NzModalService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { AccountPpe } from 'src/app/class/accountPpe';
import * as uuid from 'uuid';
import { AccountParameter } from 'src/app/class/accountParameter';
import { OrganizationService } from 'src/app/adminservices/organization.service';
import { Constants, RegExpresion, SpecialKeys } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';

@Component({
    selector: 'app-business',
    templateUrl: './business.component.html',
    styleUrls: ['./business.component.scss']
})
export class BusinessComponent implements OnInit {

    public user: any;
    public saveLoading = false;
    public confirmModal: NzModalRef;
    public appSubscriptionId: string;
    public accountPpe = new AccountPpe();
    public principalForm: FormGroup;
    public principalFormDisable: boolean;
    public link: string;
    public qrLink: string;
    public pagapeShortName = Constants.pagapeCode;
    public externalLink = ExternalLink;
    public editFlag = false;
    public applicationSubscription: ApplicationSubscription;
    public organization: any;
    constructor(
        private accountService: AccountService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private organizationService: OrganizationService,
        private modalService: NzModalService,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        private authService: AuthService) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.createPrincipalForm();
        // this.appSubscriptionId = '55d6fd21-1363-4177-85bf-60cce6423098';
        this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
            (instance: any) => {
                if (instance) {
                    this.applicationSubscription = instance.find(x => x.w0799 === this.pagapeShortName);
                    if (this.applicationSubscription) {
                        this.accountPpe.organizationId = this.applicationSubscription.w0791;
                        this.accountPpe.userCode = this.user.displayName;
                        // this.appSubscriptionId = this.applicationSuscription.w0791;
                        this.getPpeAccount(this.accountPpe.organizationId);
                        this.getDocumentTypes();
                        this.getBanks();
                    } else {
                        console.log('Suscribete');
                    }
                } else {
                    console.log('Suscribete');
                }
            }
        );
    }

    public createPrincipalForm() {
        this.principalForm = this.formBuilder.group({
            userName: [{ value: null, disabled: true }, [Validators.required]],
            businessAddress: [{ value: null, disabled: true }, [Validators.required]],
            businessName: [{ value: null, disabled: true }, [Validators.required]],
            documentType: [null, [Validators.required]],
            documentNumber: [{ value: null, disabled: true }, [Validators.required]],
            email: [{ value: null, disabled: true }, [Validators.required, Validators.email]],
            movil: [{ value: null, disabled: true }, [Validators.required, movil()]],
            solesBank: [null, [Validators.required]],
            solesAccountNumber: [null, [Validators.required]],
            solesCci: [null, [Validators.required]],
            dolarBank: [null, [Validators.required]],
            dolarAccountNumber: [null, [Validators.required]],
            dolarCci: [null, [Validators.required]]
        });
    }

    public getPpeAccount(appSubscriptionId: string) {
        this.accountService.getAccount(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.processData(instance.Data);
                        } else {
                            this.getOrganizationByUserId(this.user.uid);
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                    }

                } else {
                    this.principalFormDisable = false;
                    this.principalForm.enable();
                }
                this.principalForm.updateValueAndValidity();

            }
        );
    }

    public getOrganizationByUserId(userId: string) {
        this.organizationService.getOrganizationByUserId(userId).subscribe(
            (instance: any) => {
                if (instance) {
                    this.organization = instance;
                    this.processOrganizationData();
                }
            }
        );
    }

    public processOrganizationData() {
        this.principalForm.setValue({
            userName: this.organization.f4472,
            businessAddress: this.organization.f4463 + ' ' + this.organization.f4464,
            businessName: this.organization.f4462,
            documentType: null,
            documentNumber: this.organization.f4470,
            email: this.organization.f4471,
            movil: this.organization.f4469,
            solesBank: null,
            solesAccountNumber: null,
            solesCci: null,
            dolarBank: null,
            dolarAccountNumber: null,
            dolarCci: null
        });
        this.principalForm.markAsDirty();
        this.principalFormDisable = false;
        this.principalForm.enable();
        this.principalForm.updateValueAndValidity();
    }

    public processData(accountPpe: any) {
        this.accountPpe = accountPpe;

        this.principalForm.setValue({
            userName: this.accountPpe.userName,
            businessAddress: this.accountPpe.businessAddress,
            businessName: this.accountPpe.businessName,
            documentType: this.accountPpe.documentType,
            documentNumber: this.accountPpe.documentNumber,
            // link: this.externalLink.PAGAPE + this.accountPpe.link,

            email: this.accountPpe.parameters.find(x => x.code === 'email') ?
                this.accountPpe.parameters.find(x => x.code === 'email').value : null,

            movil: this.accountPpe.parameters.find(x => x.code === 'movil') ?
                this.accountPpe.parameters.find(x => x.code === 'movil').value : null,

            solesBank: this.accountPpe.parameters.find(x => x.code === 'solesBank') ?
                this.accountPpe.parameters.find(x => x.code === 'solesBank').value : null,

            solesAccountNumber: this.accountPpe.parameters.find(x => x.code === 'solesAccountNumber') ?
                this.accountPpe.parameters.find(x => x.code === 'solesAccountNumber').value : null,

            solesCci: this.accountPpe.parameters.find(x => x.code === 'solesCci') ?
                this.accountPpe.parameters.find(x => x.code === 'solesCci').value : null,

            dolarBank: this.accountPpe.parameters.find(x => x.code === 'dolarBank') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarBank').value : null,

            dolarAccountNumber: this.accountPpe.parameters.find(x => x.code === 'dolarAccountNumber') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarAccountNumber').value : null,

            dolarCci: this.accountPpe.parameters.find(x => x.code === 'dolarCci') ?
                this.accountPpe.parameters.find(x => x.code === 'dolarCci').value : null
        });
        this.qrLink = this.externalLink.PAGAPE + this.accountPpe.link;
        this.link = this.externalLink.PAGAPE + this.accountPpe.link;

        this.principalForm.markAsDirty();
        this.principalForm.updateValueAndValidity();
        this.principalFormDisable = true;
        this.principalForm.disable();
    }

    public editConfiguration() {
        this.editFlag = !this.editFlag;
        if (this.principalFormDisable) {
            this.principalForm.enable();
            this.principalFormDisable = false;
        } else {
            this.principalForm.disable();
            this.principalFormDisable = true;
        }
        this.principalForm.updateValueAndValidity();
    }

    public getDocumentTypes() {
    }

    public getBanks() {
    }

    public showConfirm(): void {
        this.saveLoading = true;
        this.confirmModal = this.modalService.confirm({
            nzTitle: this.translate.instant('ADMIN.CONFIGURATION.SAVE_CONFIGURATION'),
            nzContent: this.translate.instant('ADMIN.CONFIGURATION.MODAL_CREATE_CONFIGURATION'),
            nzOkText: this.translate.instant('ADMIN.MODAL_SAVE_TEXT'),
            nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
            nzOnOk: () => {
                if (this.editFlag) {
                    this.updateConfiguration();
                } else {
                    this.saveConfiguration();
                }
            },
            nzOnCancel: () => {
                this.saveLoading = false;
            }
        });
    }

    public accountAddParameter(code: string, value: string) {
        if (value) {
            const parameter = new AccountParameter();
            parameter.code = code;
            parameter.value = value;
            this.accountPpe.parameters.push(parameter);
        }
    }

    public saveConfiguration() {
        this.accountPpe.userName = this.userName.value;
        this.accountPpe.documentType = this.documentType.value;
        this.accountPpe.documentNumber = this.documentNumber.value;
        this.accountPpe.businessName = this.businessName.value;
        this.accountPpe.businessAddress = this.businessAddress.value;
        this.accountPpe.link = uuid.v4();

        this.accountAddParameter('email', this.email.value);
        this.accountAddParameter('movil', this.movil.value);
        this.accountAddParameter('solesBank', this.solesBank.value);
        this.accountAddParameter('solesAccountNumber', this.solesAccountNumber.value);
        this.accountAddParameter('solesCci', this.solesCci.value);
        this.accountAddParameter('dolarBank', this.dolarBank.value);
        this.accountAddParameter('dolarAccountNumber', this.dolarAccountNumber.value);
        this.accountAddParameter('dolarCci', this.dolarCci.value);

        this.accountService.saveAccount(this.accountPpe).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        this.link = this.externalLink.PAGAPE + this.accountPpe.link;
                        // this.principalForm.patchValue({
                        //     link: this.externalLink.PAGAPE + this.accountPpe.link
                        // });
                        this.qrLink = this.externalLink.PAGAPE + this.accountPpe.link;
                        this.modalService.success({
                            nzTitle: this.translate.instant('MODAL.SUCCESS'),
                            nzContent: this.translate.instant('MODAL.SUCCESS_MESSAGE')
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }

                } else {
                    this.modalService.warning({
                        nzTitle: 'Warning',
                        nzContent: instance.message
                        // nzContent: 'La operación se ha realizado exitosamente'
                    });
                }
                this.saveLoading = false;
                this.disableForms();
            },
            (error: any) => {
                this.saveLoading = false;
            }
        );
    }

    public updateConfiguration() {
        this.saveLoading = false;
        this.disableForms();
        this.editFlag = !this.editFlag;
    }

    public disableForms() {
        this.principalForm.disable();
        this.principalFormDisable = true;
    }

    public updateDocumentValidator(event: Event) {

        switch (Number(event)) {
            case 1:
                this.documentNumber.setValidators([Validators.required, dniDocument()]);
                break;
            case 2:
                this.documentNumber.setValidators([Validators.required, rucDocument()]);
                break;
            case 3:
            case 4:
                this.documentNumber.setValidators([Validators.required, document()]);
                break;
            default:
                break;
        }
        this.documentNumber.markAsDirty();
        this.documentNumber.updateValueAndValidity();
    }

    public updateSolesBankValidator(event: Event) {
        if (this.solesAccountNumber.value || this.solesCci.value) {

            this.solesBank.setValidators([Validators.required]);
            this.solesAccountNumber.setValidators([Validators.required, accountNumber()]);
            this.solesCci.setValidators([Validators.required, accountNumber()]);

            this.solesBank.markAsDirty();
            this.solesAccountNumber.markAsDirty();
            this.solesCci.markAsDirty();
            this.solesBank.updateValueAndValidity();
            this.solesAccountNumber.updateValueAndValidity();
            this.solesCci.updateValueAndValidity();
        }

        if (this.solesAccountNumber.value
            && this.solesCci.value
            && !this.dolarAccountNumber.value
            && !this.dolarCci.value) {

            this.dolarBank.setValidators(null);
            this.dolarAccountNumber.setValidators(null);
            this.dolarCci.setValidators(null);

            this.dolarBank.markAsDirty();
            this.dolarAccountNumber.markAsDirty();
            this.dolarCci.markAsDirty();
            this.dolarBank.updateValueAndValidity();
            this.dolarAccountNumber.updateValueAndValidity();
            this.dolarCci.updateValueAndValidity();

        }
        if (!this.solesAccountNumber.value
            && !this.solesCci.value
            && this.dolarAccountNumber.value
            && this.dolarCci.value) {

            this.solesBank.setValidators(null);
            this.solesAccountNumber.setValidators(null);
            this.solesCci.setValidators(null);

            this.solesBank.markAsDirty();
            this.solesAccountNumber.markAsDirty();
            this.solesCci.markAsDirty();
            this.solesBank.updateValueAndValidity();
            this.solesAccountNumber.updateValueAndValidity();
            this.solesCci.updateValueAndValidity();
        }

    }
    public updateDolarBankValidator(event: Event) {
        if (this.dolarAccountNumber.value || this.dolarCci.value) {

            this.dolarBank.setValidators([Validators.required]);
            this.dolarAccountNumber.setValidators([Validators.required, accountNumber()]);
            this.dolarCci.setValidators([Validators.required, accountNumber()]);

            this.dolarBank.markAsDirty();
            this.dolarAccountNumber.markAsDirty();
            this.dolarCci.markAsDirty();
            this.dolarBank.updateValueAndValidity();
            this.dolarAccountNumber.updateValueAndValidity();
            this.dolarCci.updateValueAndValidity();

        }

        if (this.dolarAccountNumber.value
            && this.dolarCci.value
            && !this.solesAccountNumber.value
            && !this.solesCci.value) {

            this.solesBank.setValidators(null);
            this.solesAccountNumber.setValidators(null);
            this.solesCci.setValidators(null);

            this.solesBank.markAsDirty();
            this.solesAccountNumber.markAsDirty();
            this.solesCci.markAsDirty();
            this.solesBank.updateValueAndValidity();
            this.solesAccountNumber.updateValueAndValidity();
            this.solesCci.updateValueAndValidity();
        }

        if (!this.dolarAccountNumber.value
            && !this.dolarCci.value
            && this.solesAccountNumber.value
            && this.solesCci.value) {

            this.dolarBank.setValidators(null);
            this.dolarAccountNumber.setValidators(null);
            this.dolarCci.setValidators(null);

            this.dolarBank.markAsDirty();
            this.dolarAccountNumber.markAsDirty();
            this.dolarCci.markAsDirty();
            this.dolarBank.updateValueAndValidity();
            this.dolarAccountNumber.updateValueAndValidity();
            this.dolarCci.updateValueAndValidity();
        }
    }

    onlyNumbers(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.SETUP_ACCOUNT_NUMBER;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.ACCOUNT_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    // cciOnlyNumbers(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
    //     const regex: RegExp = RegExpresion.SETUP_ACCOUNT_CCI;
    //     const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

    //     const current: string = control.value;
    //     let next: string;
    //     if (event instanceof KeyboardEvent) {
    //         if (specialKeys.indexOf(event.key) !== -1) {
    //             return;
    //         }
    //         next = current.concat(event.key);
    //     } else {
    //         event.preventDefault();
    //         return;
    //     }
    //     if ((next && !String(next).match(regex)) || (next && next.length > Constants.CCI_MAX_LENGTH)) {
    //         event.preventDefault();
    //     }
    // }

    get userName() {
        return this.principalForm.get('userName');
    }
    get documentType() {
        return this.principalForm.get('documentType');
    }
    get documentNumber() {
        return this.principalForm.get('documentNumber');
    }
    get email() {
        return this.principalForm.get('email');
    }
    get movil() {
        return this.principalForm.get('movil');
    }
    get businessName() {
        return this.principalForm.get('businessName');
    }
    get businessAddress() {
        return this.principalForm.get('businessAddress');
    }
    get solesBank() {
        return this.principalForm.get('solesBank');
    }
    get solesAccountNumber() {
        return this.principalForm.get('solesAccountNumber');
    }
    get solesCci() {
        return this.principalForm.get('solesCci');
    }
    get dolarBank() {
        return this.principalForm.get('dolarBank');
    }
    get dolarAccountNumber() {
        return this.principalForm.get('dolarAccountNumber');
    }
    get dolarCci() {
        return this.principalForm.get('dolarCci');
    }
}

function movil(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        // const RegExp = /^+\d{9|12}$/;
        const RegExp = /^\+(\d{9}|\d{10}|\d{11}|\d{12}|\d{13})$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { movil: true };
    };
}

function accountNumber(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{17}$/;
        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { accountNumber: true };
    };
}

function dniDocument(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{8}$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { dniDocument: true };
    };
}

function rucDocument(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d{11}$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { rucDocument: true };
    };
}

function document(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^(\d{9}|\d{10}|\d{12}|\d{13})$/;

        if (control.value && control.value.match(RegExp)) {
            return null;
        }
        return { rucDocument: true };
    };
}
