import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupRoutingModule } from './setup-routing.module';
import { NgZorroAntdModule, NzUploadModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { ServiceComponent } from './service/service.component';
import { BusinessComponent } from './business/business.component';


@NgModule({
  declarations: [ServiceComponent, BusinessComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    NzUploadModule,
    FormsModule,
    TranslateModule.forChild(),
    SetupRoutingModule,
    ReactiveFormsModule,
    QRCodeModule
  ]
})
export class SetupModule { }
