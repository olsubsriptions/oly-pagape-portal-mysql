import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceComponent } from './service/service.component';
import { BusinessComponent } from './business/business.component';


const routes: Routes = [
  {
    path: 'business',
    component: BusinessComponent
  },
  {
    path: 'service',
    component: ServiceComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }
