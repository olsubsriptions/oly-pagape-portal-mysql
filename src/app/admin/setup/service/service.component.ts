import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { NzNotificationService, NzModalService, NzModalRef } from 'ng-zorro-antd';
import { UploadFile } from 'ng-zorro-antd/upload';
import { TranslateService } from '@ngx-translate/core';
import { AccountService } from 'src/app/adminservices/account.service';
import { LinkData } from 'src/app/class/linkData';
import { Constants } from 'src/app/class/constants';
import { AuthService } from 'src/app/core/auth.service';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { PaymentLinkDefault } from 'src/app/class/paymentLinkDefault';


@Component({
    selector: 'app-service',
    templateUrl: './service.component.html',
    styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {

    public user: any;
    public paymentlinkForm: FormGroup;
    public style = {
        display: 'block',
        height: '30px',
        lineHeight: '30px'
    };
    public logo: File;
    public loading = false;
    public logoUrl?: string;
    public uploading = false;
    public saveLoading = false;
    public confirmModal: NzModalRef;
    public paymentLinkDefault = new PaymentLinkDefault();
    public pagapeShortName = Constants.pagapeCode;
    public applicationSubscription: ApplicationSubscription;
    public accountCurrencyList = new Array<string>();

    fileList: UploadFile[] = [];

    constructor(
        private authService: AuthService,
        private accountService: AccountService,
        private translate: TranslateService,
        private nzNotification: NzNotificationService,
        private modalService: NzModalService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private formBuilder: FormBuilder) {

    }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.createPaymentlinkForm();
        this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
            (instance: any) => {
                if (instance) {
                    this.applicationSubscription = instance.find(x => x.w0799 === this.pagapeShortName);
                    if (this.applicationSubscription) {
                        this.paymentLinkDefault.organizationId = this.applicationSubscription.w0791;
                        this.paymentLinkDefault.userCode = this.user.displayName;
                        this.getAccountDefaultConfig(this.paymentLinkDefault.organizationId);
                        this.getAccountCurrency(this.paymentLinkDefault.organizationId);
                        this.getAccountLogo(this.paymentLinkDefault.organizationId);
                    }
                }
            }
        );
    }

    public createPaymentlinkForm() {
        this.paymentlinkForm = this.formBuilder.group({
            detail: [null, [Validators.maxLength(50)]],
            currency: [null, [Validators.required]],
            amount: [null, [currencyAmount()]],
            singlePayment: [false, [Validators.required]],
            expiration: [false, [Validators.required]],
            singleClient: [false, [Validators.required]],
            notification: [false, [Validators.required]],
            notificationType: [null]
        });
    }

    public getAccountDefaultConfig(appSubscriptionId: string) {
        this.accountService.getAccountDefaultConfig(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.processData(instance.Data.defaultConfig);
                        }
                    }
                }
            }
        );
    }

    public processData(defaultConfig: any) {

        const defaultConfigObj = JSON.parse(defaultConfig);

        this.paymentlinkForm.setValue({
            detail: defaultConfigObj.detail,
            currency: defaultConfigObj.currency,
            amount: defaultConfigObj.amount,
            singlePayment: defaultConfigObj.singlePayment,
            expiration: defaultConfigObj.expiration,
            singleClient: defaultConfigObj.singleClient,
            notification: defaultConfigObj.notification,
            notificationType: defaultConfigObj.notificationType,
        });
        this.paymentlinkForm.updateValueAndValidity();
    }

    public getAccountCurrency(appSubscriptionId: string) {
        this.accountService.getAccountCurrency(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.accountCurrencyList = instance.Data.Currency;
                        }
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            }
        );
    }

    public getAccountLogo(appSubscriptionId: string) {
        this.accountService.getAccountLogo(appSubscriptionId).subscribe(
            (instance: any) => {
                if (instance) {
                    if (instance.Result === 0) {
                        if (instance.Data) {
                            this.logoUrl = instance.Data.logoFormat + ',' + instance.Data.logoBase64;
                        }
                    }
                }
            }
        );
    }

    public showConfirm(): void {
        this.saveLoading = true;
        this.confirmModal = this.modalService.confirm({
            nzTitle: this.translate.instant('ADMIN.CONFIGURATION.SAVE_DEFAULTS'),
            nzContent: this.translate.instant('ADMIN.CONFIGURATION.MODAL_CREATE_CONFIGURATION'),
            nzOkText: this.translate.instant('ADMIN.MODAL_SAVE_TEXT'),
            nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
            nzOnOk: () => {
                this.savePaymentLink();
            },
            nzOnCancel: () => {
                this.saveLoading = false;
            }
        });
    }

    public savePaymentLink() {

        const defaultConfig = new LinkData();
        defaultConfig.detail = this.detail.value;
        defaultConfig.currency = this.currency.value;
        defaultConfig.amount = this.amount.value;
        defaultConfig.singlePayment = this.singlePayment.value;
        defaultConfig.expiration = this.expiration.value;
        defaultConfig.singleClient = this.singleClient.value;
        defaultConfig.notification = this.notification.value;
        defaultConfig.notificationType = this.notification.value ? this.notificationType.value : null;

        this.paymentLinkDefault.defaultConfig = JSON.stringify(defaultConfig);
        this.accountService.saveAccountDefaultConfig(this.paymentLinkDefault).subscribe(
            (instance: any) => {
                if (instance.Result === 0) {
                    this.nzNotification.create(
                        'success',
                        this.translate.instant('MODAL.SUCCESS'),
                        this.translate.instant('MODAL.SUCCESS_MESSAGE'),
                    );
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
                this.saveLoading = false;
            },
            (error: any) => {
                this.saveLoading = false;
            }
        );
    }

    beforeUpload = (file: File): boolean => {
        this.fileList = [];
        this.loading = true;
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            this.nzNotification.create(
                'error',
                this.translate.instant('MODAL.ERROR'),
                this.translate.instant('MODAL.INVALID_IMAGE'),
            );
            this.loading = false;
            return false;
        }
        const isLt2M = file.size! / 1024 / 1024 < 2;
        if (!isLt2M) {
            this.nzNotification.create(
                'error',
                this.translate.instant('MODAL.ERROR'),
                this.translate.instant('MODAL.IMAGE_TOO_BIG'),
            );
            this.loading = false;
            return false;
        }

        this.logo = file;
        this.getBase64(file, (img: string) => {
            this.logoUrl = img;
            this.loading = false;
        });
        return false;
    }

    handleUpload(): void {

        this.uploading = true;
        if (this.logo) {
            console.log('uploading');
            const saveLogo = {
                organizationId: this.paymentLinkDefault.organizationId,
                logoFormat: this.logoUrl.split(',')[0],
                logoBase64: this.logoUrl.split(',')[1]
            }
            this.accountService.saveAccountLogo(saveLogo).subscribe(
                (instance: any) => {
                    if (instance.Result === 0) {
                        this.nzNotification.create(
                            'success',
                            this.translate.instant('MODAL.SUCCESS'),
                            this.translate.instant('MODAL.SUCCESS_MESSAGE'),
                        );
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                    this.uploading = false;
                }
            );
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('MODAL.SELECT_IMAGE'),
            );
            this.uploading = false;
            return;
        }
    }

    private getBase64(img: File, callback: (img: string) => void): void {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result!.toString()));
        reader.readAsDataURL(img);
    }

    get detail() {
        return this.paymentlinkForm.get('detail');
    }
    get currency() {
        return this.paymentlinkForm.get('currency');
    }
    get amount() {
        return this.paymentlinkForm.get('amount');
    }
    get singlePayment() {
        return this.paymentlinkForm.get('singlePayment');
    }
    get expiration() {
        return this.paymentlinkForm.get('expiration');
    }
    get expirationDate() {
        return this.paymentlinkForm.get('expirationDate');
    }
    get expirationTime() {
        return this.paymentlinkForm.get('expirationTime');
    }
    get singleClient() {
        return this.paymentlinkForm.get('singleClient');
    }
    get notification() {
        return this.paymentlinkForm.get('notification');
    }
    get notificationType() {
        return this.paymentlinkForm.get('notificationType');
    }
}

function currencyAmount(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        const RegExp = /^\d+\.(\d{2})+$/;
        if (!control.value || (control.value && control.value.match(RegExp))) {
            return null;
        }
        return { currencyAmount: true };
    };
}
