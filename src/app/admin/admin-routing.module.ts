import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '../core/auth.guard';
import { AuthGuardSub } from '../core/auth.guard.sub';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    // canActivate: [AuthGuard]
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        canActivate: [AuthGuard],
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'billing',
        canActivate: [AuthGuard],
        loadChildren: () => import('./billing/billing.module').then(m => m.BillingModule),
      },
      // {
      //   path: 'support',
      //   canActivate: [AuthGuard],
      //   loadChildren: () => import('./support/support.module').then(m => m.SupportModule),
      // },
      {
        path: 'setup',
        canActivate: [AuthGuard],
        loadChildren: () => import('./setup/setup.module').then(m => m.SetupModule),
      },
      {
        path: 'paymentlink',
        canActivate: [AuthGuard],
        loadChildren: () => import('./paymentlink/paymentlink.module').then(m => m.PaymentlinkModule),
      },
      {
        path: 'profile',
        canActivate: [AuthGuard],
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
      },
      {
        path: 'subscription',
        canActivate: [AuthGuardSub],
        loadChildren: () => import('./subscription/subscription.module').then(m => m.SubscriptionModule),
      },
      {
        path: 'help',
        canActivate: [AuthGuardSub],
        loadChildren: () => import('./help/help.module').then(m => m.HelpModule),
      }
    ]
  },
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
