import { Injectable } from '@angular/core';
import { Constants } from '../class/constants';
import { ExternalLink } from '../class/enums';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    public element: HTMLElement;

    constructor() { }

    public currencyNumberToWord(amount: string): string {
        let numberInLetters;
        const amountNumber = parseFloat(amount).toFixed(2).toString();
        // if (amount === '0.00' || amount === '0') {
        if (amountNumber === '0.00' || amountNumber === '0') {
            return 'Cero con 00/100';
        } else {
            const splitedAmount = amountNumber.split('.');
            const decimalPart = splitedAmount[1];
            const integerPart = Number(splitedAmount[0]);

            numberInLetters = this.millones(integerPart);
            numberInLetters = numberInLetters.replace('Uno Mil', 'Un Mil');
            return (numberInLetters + 'con ' + decimalPart + '/100');
        }
    }

    public ordinalNumberToWord(num: string): string {
        let numberInLetters;
        const amountNumber = parseInt(num, 10).toString();
        if (amountNumber === '0') {
            return 'Cero';
        } else {
            numberInLetters = this.millones(Number(amountNumber));
            numberInLetters = numberInLetters.replace('Uno Mil', 'Mil');
            return numberInLetters;
        }
    }

    private millones(num: number): string {
        const divisor = 1000000;
        const cientos = Math.floor(num / divisor);
        const resto = num - (cientos * divisor);

        const strMillones = this.seccion(num, divisor, 'Un Millón ', 'Millones ');
        const strMiles = this.miles(resto);

        if (strMillones === '') {
            return strMiles;
        }

        return strMillones + strMiles;
    }

    private miles(num: number) {
        const divisor = 1000;
        const cientos = Math.floor(num / divisor);
        const resto = num - (cientos * divisor);

        const strMiles = this.seccion(num, divisor, 'Mil ', 'Mil ');
        const strCentenas = this.centenas(resto);

        if (strMiles === '') {
            return strCentenas;
        }

        return strMiles + strCentenas;
    }

    private seccion(num: number, divisor: number, strSingular, strPlural) {
        const cientos = Math.floor(num / divisor);
        const resto = num - (cientos * divisor);

        let letras = '';

        if (cientos > 0) {
            if (cientos > 1) {
                letras = this.centenas(cientos) + strPlural;
            } else {
                letras = strSingular;
            }
        }
        if (resto > 0) {
            letras += '';
        }
        return letras;
    }

    private centenas(num: number) {
        const centenas = Math.floor(num / 100);
        const decenas = num - (centenas * 100);

        switch (centenas) {
            case 1:
                if (decenas > 0) {
                    return 'Ciento ' + this.decenas(decenas);
                }
                return 'Cien ';
            case 2: return 'Doscientos ' + this.decenas(decenas);
            case 3: return 'Trescientos ' + this.decenas(decenas);
            case 4: return 'Cuatrocientos ' + this.decenas(decenas);
            case 5: return 'Quinientos ' + this.decenas(decenas);
            case 6: return 'Seiscientos ' + this.decenas(decenas);
            case 7: return 'Setecientos ' + this.decenas(decenas);
            case 8: return 'Ochocientos ' + this.decenas(decenas);
            case 9: return 'Novecientos ' + this.decenas(decenas);
        }

        return this.decenas(decenas);
    }

    private decenas(num: number) {

        const decena = Math.floor(num / 10);
        const unidad = num - (decena * 10);

        switch (decena) {
            case 1:
                switch (unidad) {
                    case 0: return 'Diez ';
                    case 1: return 'Once ';
                    case 2: return 'Doce ';
                    case 3: return 'Trece ';
                    case 4: return 'Catorce ';
                    case 5: return 'Quince ';
                    default: return 'Dieci' + this.unidades(unidad).toLowerCase();
                }
            case 2:
                switch (unidad) {
                    case 0: return 'Veinte ';
                    default: return 'Veinti' + this.unidades(unidad).toLowerCase();
                }
            case 3: return this.decenasY('Treinta ', unidad);
            case 4: return this.decenasY('Cuarenta ', unidad);
            case 5: return this.decenasY('Cincuenta ', unidad);
            case 6: return this.decenasY('Sesenta ', unidad);
            case 7: return this.decenasY('Setenta ', unidad);
            case 8: return this.decenasY('Ochenta ', unidad);
            case 9: return this.decenasY('Noventa ', unidad);
            case 0: return this.unidades(unidad);
        }
    }

    private unidades(num: number) {

        switch (num) {
            case 1: return 'Un ';
            case 2: return 'Dos ';
            case 3: return 'Tres ';
            case 4: return 'Cuatro ';
            case 5: return 'Cinco ';
            case 6: return 'Seis ';
            case 7: return 'Siete ';
            case 8: return 'Ocho ';
            case 9: return 'Nueve ';
        }

        return '';
    }

    private decenasY(strSin, numUnidades) {
        if (numUnidades > 0) {
            return strSin + 'y ' + this.unidades(numUnidades);
        }
        return strSin;
    }

    public hideData(data: string): string {
        let response = null;
        if (data) {
            response = data.slice(0, 4) + '********' + data.slice(-3);
        }
        return response;
    }

    public formatNumber(value: string, digits: number): string {
        const text = '0000000000000000';
        return (text + value).slice(-digits);
    }

    public formatCurrency(value: number): string {

        return Number(Math.round(parseFloat(value + 'e' + Constants.DECIMAL_PLACES)) + 'e-' + Constants.DECIMAL_PLACES)
            .toFixed(Constants.DECIMAL_PLACES);
    }

    public wordTruncate(value: string, length: number): string {
        return value.substring(0, length) + '...';
    }

    public getQrWidth(): string {
        this.element = document.getElementById('admin') as HTMLElement;
        const width = this.element.offsetWidth;
        const newWidth = width * 68 / 100;
        return width > 550 ? '400' : newWidth + '';
    }

    public shareWhatsapp(value: string) {

        const mobile = this.isMobile();

        if (mobile) {
            window.location.href = ExternalLink.WHATSAPP_MOVIL + value;
        } else {
            window.open(ExternalLink.WHATSAPP_WEB + value, '_blank');
        }
    }

    public shareFacebook(value: string) {
        window.open(ExternalLink.FACEBOOK + value, '_blank');
    }

    public shareTwitter(value: string) {
        window.open(ExternalLink.TWITTER + encodeURIComponent(value), '_blank');
    }

    private isMobile(): boolean {
        return /iPhone|webOS|BlackBerry|Windows Phone|iPad|iPod|Android/i.test(navigator.userAgent);
    }

}
