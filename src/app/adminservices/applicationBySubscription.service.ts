import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicationSubscription } from '../class/applicationSubscription';

@Injectable({
    providedIn: 'root'
})

export class ApplicationBySubscriptionService {

    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public getApplicationsBySubscription(userId: string): Observable<ApplicationSubscription> {
        const endpoint = this.consoleUrl + '/applications/subscriptions'
            + '?userId=' + userId;
        return this.http.get<ApplicationSubscription>(endpoint);
    }

    public register(applicationSubscription: ApplicationSubscription): Observable<any> {
        const endpoint = this.consoleUrl + '/applications/subscriptions/pagape';
        return this.http.post<ApplicationSubscription>(endpoint, applicationSubscription);
    }
}
