import { Injectable } from '@angular/core';
import * as Crypto from 'crypto-js';

@Injectable({
    providedIn: 'root'
})

export class CryptoService {

    public pksys = '308204bc020100300d06092a864886f70d0101010500048204a6308204a20201000282010100b2481b731cd2aaf3979e7c59b49c41f90b7ac1727930ecf6ccf035376ecd5531c0d936337d3a72a83c21fcb163a851ed7949e32aa72a206c453f16727fbce1794d354079875af5b28d42610e0e7052344c5c99512675c1769555602722baf19976a620648a619f01af560afcfedf2ff1066a7314bbca84c3bbc98c43b70a955074add1f0b0104b71aa5718d9062de0d7d148f1132b03bcb84a077d358865fb5197c115d7db2a3f83aac23f79682251b32945756ff0855407ae814a505706d273f66d828b731778bc0af69bbea2cf2c96fb78587a1ae4539fb59efb067018022f0284240d59fa7b12a939ceb339f591a33c5977cb273202bf8778a8b2867714910203010001028201004f0f8914c05870353c2cad4a7ebcb8c8ff9d3d824d5a340bb2906799208af057f304a073aefa277491ce24a869e0700c96b7fc84a2c31cc3c6e59647ca7bf1a58066b823ecb19ab9c6c9040f5eb9d2ca9c7e880323c21ebc3eb730bfca380b021a4acb9caa7d662d321b3f1e38ccb30ef8e0660e70c4fba16d7a90b84ac8e7b39ac2d2dc5b682da7a2410a2bc95be649548a5763e71c4604fd1433748a47cdedcce4f399cd831c4e9f1ea55dfc12c880c1cb543240a11f4e0de17b3508e763e43fbc626bfe76ec63df9f947e2b35e4b8a485db3db05eb0fdae7a2da02eebdab8352f2e3a371baee9ce1c1b64dcc4c8555ac203244f2cb5ea0992c97139c4f59102818100ec97c0468c0ada65f251e16b2e0961814d0aa92d845e2c0c517f173035e4c27fb368e09ad8c7bdada204985c273e411d772ab269653de3d250748af44c4edc902817b29ceedbd6579db3a85ba123fbdb3495701aab84b0f174470d23fa85a273eb91c4ddfffb05d2fa154b420eb9c05002ce2529403b63b968c9a7f3cfb4ebb502818100c0e7df3c11653de350b57ccc128b9cbd10eac9417e0546cf83168b6a39b4b91e530f66d98451aac0f1656589e25c524cc256773040ec7295cf39906a2daf34eae1e92adba65b8c73c56094d2494a283604b7a003efb115835f2046cb3e82835a0577a61b6c839eabacf0f883f4b423e4f1327b4c57ca66e8510b064fd28c26ed0281806c4d4b07003437a553378828b7fa33582d1c243561d279f76996f3d66dd539f39afcf753eac8045be73e93401bf6b377f984e206bd0187b84e7c0adfa9ceafc88d91cb816b15d71864684939289a803fe7e2405ef504d8baaeaf97a45099bbb3b4b2159c4a016eb915218a41c2dbe4dcbb6c0236cfe05fd60fd1bfc02be475c1028180659676903a94f7778b5c58215c2edbd7115ea3a7f2c6b298bb6f16f254fb6075a0c63ea2943531d83407f2e61f28014b5d188de2027ec28534411b555f43a2798388f841fe24be1d85b72c0866024094d9fd1dae94fecbd688350633287330db2a7fed210a0f7272f43b5e94973c7a9ea98aad35f211ac09f20bfd41f21e4ead0281803d782538e66c2a105dc26d20eec083f7a019e836b7766962ccdc224f16db7b2b4f0e0aa279b3b0f763eaabaaf51bdb327016c41b46780b21783d19adb9a69105fb52134ec8ffc5fafd045f6212f82f052eb5e4b08f6130e6d1f74144ca8614bf12eea797de6142bfde2b1ab7b807bce9fe280cfdc2a0aea97f0241bde7693271'
    // public securityKey = '1a53f1d1c98ef942ecaf1c5871a42be9ea77b7c01de1609758dc9d7a37163e726119947c8339ae597cce360b14b9fdb62c4535e4f56db7f96df73244774df328f71cf7f0e2cf8bdb6c3800ea5253072d4c8c68cd27cf66818eb958ca009f7045fdb3700bd7a566e37a964dfc5de6bd41';
    public securityKey = '3b737d71ceafff1987234621b0f3abcedfb1e368a27f2a7412072958d547a478c4b562e3dd0d288f557ac43aef36e560b201040a373d654f8c94eff1e1fe06a45130892de372bebc6e79531b935d0e91607cd35753d025c3676f2d46d9fe077fe012f018f6469ef2cd1d07c486bb9700';
    public keySize = 256;
    public iterations = 65536;
    public salt = Crypto.lib.WordArray.create(new Int8Array([-93, 17, 45, 78, -103, -82, 52, -24]));
    public iv = Crypto.lib.WordArray.create(new Int8Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]));

    constructor() { }

    getHpksys() {
        return Crypto.SHA384(this.pksys).toString();
    }

    getPkey() {
        return Crypto.SHA384('b30348ed-0730-11eb-9c57-000c298bb43c').toString();
    }

    getProtectedKey(){
        return this.decrypt(this.getHpksys(), this.securityKey);
    }

    aesEncrypt(value: string) {
        return this.encrypt(this.getProtectedKey(), value);
    }

    aesDecrypt(value: string) {
        return this.decrypt(this.getProtectedKey(), value);
    }

    encrypt(secretKey: string, value: string) {
        const key = Crypto.PBKDF2(secretKey, this.salt, {
            keySize: this.keySize / 32,
            hasher: Crypto.algo.SHA256, // compatibilidad con PBKDF2WithHmacSHA256 de java
            iterations: this.iterations
        });

        const encrypted = Crypto.AES.encrypt(value, key, {
            iv: this.iv,
            padding: Crypto.pad.Pkcs7,
            mode: Crypto.mode.CBC
        });

        return Crypto.enc.Hex.stringify(encrypted.ciphertext);
    }

    decrypt(secretKey: string, value: string) {

        const key = Crypto.PBKDF2(secretKey, this.salt, {
            keySize: this.keySize / 32,
            hasher: Crypto.algo.SHA256, // compatibilidad con PBKDF2WithHmacSHA256 de java
            iterations: this.iterations
        });

        const cipher = {
            ciphertext: Crypto.enc.Hex.parse(value)
        };

        const decrypted = Crypto.AES.decrypt(cipher, key, {
            iv: this.iv,
            padding: Crypto.pad.Pkcs7,
            mode: Crypto.mode.CBC
        });

        return Crypto.enc.Utf8.stringify(decrypted);
    }
}
