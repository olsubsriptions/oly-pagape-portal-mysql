import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TtsRequest } from '../class/ttsRequest';
import { AccountPpe } from '../class/accountPpe';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Transactions } from '../class/enums';
import { PaymentLinkDefault } from '../class/paymentLinkDefault';

@Injectable({
    providedIn: 'root'
})

export class AccountService {

    private ttsUrl = environment.tts.url;
    private transactions = Transactions;
    constructor(private http: HttpClient) {
    }

    public getAccount(appSubscriptionId: string): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_MERCHANTDATA;
        request.TTSName = 'GetMerchantData';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public saveAccount(account: AccountPpe): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.SAVE_MERCHANTDATA;
        request.TTSName = 'SaveMerchantData';
        request.appID = '1';
        request.Data = account;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public saveAccountLogo(saveLogo: any): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3015;
        request.TTSName = 'SaveMerchantLogo';
        request.appID = '1';
        request.Data = saveLogo;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }
    public getAccountLogo(appSubscriptionId: string): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3013;
        request.TTSName = 'GetMerchantLogo';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public saveAccountDefaultConfig(paymentLinkDefault: PaymentLinkDefault): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3014;
        request.TTSName = 'SaveMerchantDefaultConfig';
        request.appID = '1';
        request.Data = paymentLinkDefault;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getAccountDefaultConfig(appSubscriptionId: string): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3012;
        request.TTSName = 'GetMerchantDefaultConfig';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getAccountCurrency(appSubscriptionId: string): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3011;
        request.TTSName = 'GetMerchantCurrency';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getAccountBanks(country: string): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = 3021;
        request.TTSName = 'GetBanks';
        request.appID = '1';
        request.Data = {};
        request.Data.country = 'PE';

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public updateUserInfo(account: AccountPpe): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.UPDATE_MERCHANTDATA;
        request.TTSName = 'UpdateMerchantData';
        request.appID = '1';
        request.Data = account;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public updateBankInfo(account: AccountPpe): Observable<TtsRequest> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.UPDATE_PROPMERCHANTDATA;
        request.TTSName = 'UpdatePropMerchantData';
        request.appID = '1';
        request.Data = account;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }
}
