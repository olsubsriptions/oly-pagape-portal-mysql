import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Transactions } from '../class/enums';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TtsRequest } from '../class/ttsRequest';

@Injectable({
    providedIn: 'root'
})

export class NotificationService {

    private ttsUrl = environment.tts.url;
    private consoleUrl = environment.console.url;
    private transactions = Transactions;

    constructor(private http: HttpClient) {
    }

    // public sendSms(country: string, phoneNumber: string, validationCode: string): Observable<any> {
    public sendSms(data: any): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.SEND_SMS;
        // request.TTSCode = 9999;
        request.TTSName = 'sendSms';
        request.appID = '1';
        request.Data = data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    // public sendEmail(email: string, data: string): Observable<any> {
    public sendEmail(data: any): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.SEND_EMAIL;
        request.TTSName = 'sendEmail';
        request.appID = '1';
        request.Data = data;
        // request.Data = {};
        // request.Data.to = email;
        // request.Data.template = 'Su link de verificación es: ' + this.consoleUrl + '/v?cid=' + data;
        // request.Data.link = this.consoleUrl + '/v?cid=' + data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public sendNotification(notification: any): Observable<any> {
        return null;
    }
}
