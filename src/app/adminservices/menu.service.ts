import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

const MenuLogout = {
    label: 'ADMIN.MENU.LOGOUT',
    type: 'menu',
    icon: 'logout',
    url: ''
};

const MenuItems = [
    {
        label: 'ADMIN.MENU.HOME',
        type: 'menu',
        icon: 'home',
        url: '/admin/home',
    },
    {
        label: 'ADMIN.MENU.PAYMENT_LINK',
        type: 'sub',
        icon: 'link',
        open: false,
        childrens: [
            {
                label: 'ADMIN.MENU.GENERATE_PAYMENT_LINK',
                url: '/admin/paymentlink/generate',
            },
            {
                label: 'ADMIN.MENU.MY_PAYMENT_LINKS',
                url: '/admin/paymentlink/links',
            }
        ]
    },
    {
        label: 'ADMIN.MENU.PAYMENTS_HISTORY',
        type: 'menu',
        icon: 'credit-card',
        url: '/admin/billing/payments',
    },
    {
        label: 'ADMIN.MENU.PROFILE',
        type: 'menu',
        icon: 'user',
        url: '/admin/profile',
        highlight: true,
    },
    {
        label: 'ADMIN.MENU.HELP',
        type: 'menu',
        icon: 'question-circle',
        url: '/admin/help',
    }
];

@Injectable({
    providedIn: 'root'
})

export class MenuService {

    private menuLogout = JSON.parse(JSON.stringify(MenuLogout));
    private menuItems = JSON.parse(JSON.stringify(MenuItems));

    constructor(
        private translate: TranslateService
    ) { }

    public resetMenuItems() {
        this.menuLogout = JSON.parse(JSON.stringify(MenuLogout));
        this.menuItems = JSON.parse(JSON.stringify(MenuItems));
    }

    public getMenuLogout() {
        return this.menuLogout;
    }

    public getMenuItems() {
        return this.menuItems;
    }

    public setSelectedMenu(url: string) {
        this.resetMenuItems();
        this.menuItems.forEach(menuItem => {
            if (menuItem.childrens) {
                menuItem.childrens.forEach(children => {
                    if (children.url === url) {
                        menuItem.open = true;
                        return;
                    }
                });
            }
        });
    }
}
