import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SubscriptionService {
    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public getSubscriptionByUserId(userId: any): Observable<any> {
        const endpoint = this.consoleUrl + '/subscriptions'
            + '?userId=' + userId;
        return this.http.get<any>(endpoint);
    }
}
