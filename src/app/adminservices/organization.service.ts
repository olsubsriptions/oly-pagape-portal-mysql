import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Organization } from '../class/organization';
import { Transactions } from '../class/enums';
import { TtsRequest } from '../class/ttsRequest';

@Injectable({
    providedIn: 'root'
})

export class OrganizationService {

    private consoleUrl = environment.console.url;
    private ttsUrl = environment.tts.url;
    private transactions = Transactions;
    constructor(private http: HttpClient) {
    }

    public getOrganizationByUserId(userId: string): Observable<Organization> {
        const endpoint = this.consoleUrl + '/organization'
            + '?userId=' + userId;
        return this.http.get<Organization>(endpoint);
    }

    public validateOrganizationDocument(document: string): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = 9999;
        request.TTSName = 'validateDoc';
        request.appID = '1';
        request.Data = {};
        request.Data.document = document;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }
}
