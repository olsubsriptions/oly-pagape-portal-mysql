import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TtsRequest } from '../class/ttsRequest';
import { Observable, of } from 'rxjs';
import { Pagination } from '../class/pagination';
import { HttpClient } from '@angular/common/http';
import { Transactions } from '../class/enums';

@Injectable({
    providedIn: 'root'
})

export class PaymentService {

    private ttsUrl = environment.tts.url;
    private transactions = Transactions;

    constructor(private http: HttpClient) {
    }

    public getPayments(appSubscriptionId: string, pagination: Pagination): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_TRANSACTIONS;
        request.TTSName = 'getPayments';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;
        request.Data.pag = pagination.pag;
        request.Data.regxpag = pagination.regxpag;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getTransactions(data: any): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_LAST_TRANSACTIONS;
        request.TTSName = 'getLastTransactions';
        request.appID = '1';
        request.Data = data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getPaymentsInfo(data: any): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_TRANSACTIONS_INFO;
        request.TTSName = 'getTransactionInfo';
        request.appID = '1';
        request.Data = data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getPaymentsStats(appSubscriptionId: string): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_PAYMENTS_STATS;
        request.TTSName = 'getPaymentsStats';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = appSubscriptionId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public getHistoricPaymentsInfo(data: any): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_HISTORIC_TRANSACTIONS_INFO;
        request.TTSName = 'getHistoricTransactionInfo';
        request.appID = '1';
        request.Data = data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }
}