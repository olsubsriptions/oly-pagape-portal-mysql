import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TtsRequest } from '../class/ttsRequest';
import { PaymentLink } from '../class/paymentLink';
import { HttpClient } from '@angular/common/http';
import { Pagination } from '../class/pagination';
import { Transactions } from '../class/enums';

@Injectable({
    providedIn: 'root'
})

export class PaymentlinkService {

    private ttsUrl = environment.tts.url;
    private transactions = Transactions;
    constructor(private http: HttpClient) {
    }

    public getPaymentLinks(data: any): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_PAYMENTLINKS;
        request.TTSName = 'getPaymentLinks';
        request.appID = '1';
        request.Data = data;

        return this.http.post<TtsRequest>(this.ttsUrl, request);

    }

    public getPaymentLinksStats(organizationId: string): Observable<any>{

        const request = new TtsRequest();
        request.TTSCode = this.transactions.GET_PAYMENTLINKS_STATS;
        request.TTSName = 'getPaymentLinksStats';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = organizationId;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public savePaymentLink(paymentLink: PaymentLink): Observable<any> {

        const request = new TtsRequest();
        request.TTSCode = this.transactions.SAVE_PAYMENTLINK;
        request.TTSName = 'savePaymentLink';
        request.appID = '1';
        request.Data = paymentLink;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public changeStatusPaymentLink(paymentLink: any): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.CHANGESTATUS_PAYMENTLINK;
        request.TTSName = 'linkStatusChange';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = paymentLink.organizationId;
        request.Data.link = paymentLink.link;
        request.Data.status = paymentLink.status;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }

    public deletePaymentLink(paymentLink: PaymentLink): Observable<any> {
        const request = new TtsRequest();
        request.TTSCode = this.transactions.DELETE_PAYMENTLINK;
        request.TTSName = 'linkDelete';
        request.appID = '1';
        request.Data = {};
        request.Data.organizationId = paymentLink.organizationId;
        request.Data.link = paymentLink.link;

        return this.http.post<TtsRequest>(this.ttsUrl, request);
    }
}
