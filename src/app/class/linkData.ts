export class LinkData {
    detail: string;
    currency: string;
    amount: string;
    freePay: boolean;
    singlePayment: boolean;
    expiration: boolean;
    expirationDate: string;
    singleClient: boolean;
    notification: boolean;
    notificationType: string;
}