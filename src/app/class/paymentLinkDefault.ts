export class PaymentLinkDefault {
    organizationId: string;
    defaultConfig: string;
    userCode: string;
}
