export class TransactionInfo {
    amountPEN: string;
    textAmountPEN: string;
    amountUSD: string;
    textAmountUSD: string;
    transactionsPEN: number;
    transactionsUSD: number;
    transactionsTotal: number;
    transactionsPercentPEN: number;
    transactionsPercentUSD: number;
}