export class PaymentLink {
    organizationId: string;
    linkData: string;
    link: string;
    userCode: string;
    status: number;
    linkType: number;
    expirationDate: string;
}