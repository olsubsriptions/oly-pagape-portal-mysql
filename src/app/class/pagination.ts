export class Pagination {
    pag: number;
    regxpag: number;
    totalrows: number;

    constructor(pag: number, regxpag: number, totalrows: number) {
        this.pag = pag;
        this.regxpag = regxpag;
        this.totalrows = totalrows;
    }
}


