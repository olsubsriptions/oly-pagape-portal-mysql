export enum MenuTypes {
    MENU = 'menu',
    SUBMENU = 'sub',
    BUTTON = 'button'
}

export enum AppComponentType {
    IVR = 1,
    SELF_SERVICE = 2,
    APP_MOVIL = 3,
    PRINCIPAL = 4
}

export enum ApplicationCode {
    MONETA = 'MNT',
    SECURITY_PROTECCION_SUITE = 'SPS',
    TRANSLINK_TRANSACTION_SERVICES = 'TTS',
    PAGA_PE = 'PPE'
}

export enum SubsComponent {
    SELF_SERVICE_URL = 1,
    IVR_DISPLAY_NAME = 2,
    IVR_USERNAME = 3,
    IVR_PASSWORD = 4,
    IVR_AUTHORIZATION_USERNAME = 5,
    IVR_DOMAIN = 6,
    IVR_PORT = 7,
    IVR_PORT_TYPE = 8
}

export enum Language {
    SPANISH = 1,
    ENGLISH = 2
}
export enum Transactions {
    GET_PAYMENTLINKS = 3002,
    SAVE_PAYMENTLINK = 3001,
    CHANGESTATUS_PAYMENTLINK = 3008,
    DELETE_PAYMENTLINK = 3009,
    GET_MERCHANTDATA = 3004,
    SAVE_MERCHANTDATA = 3003,
    GET_TRANSACTIONS = 3005,
    SEND_SMS = 8001,
    SEND_EMAIL = 9001,
    GET_TRANSACTIONS_INFO = 3016,
    GET_HISTORIC_TRANSACTIONS_INFO = 3017,
    GET_LAST_TRANSACTIONS = 3018,
    GET_PAYMENTLINKS_STATS = 3019,
    GET_PAYMENTS_STATS = 3020,
    UPDATE_MERCHANTDATA = 3022,
    UPDATE_PROPMERCHANTDATA = 3023
}

export enum Status {
    ENABLED = 1,
    DISABLED = 0
}

export enum ExternalLink {
    PAGAPE = 'https://pagos.paga.pe?cid=',
    WHATSAPP_WEB = 'https://web.whatsapp.com/send?text=',
    WHATSAPP_MOVIL = 'whatsapp://send?text=',
    FACEBOOK = 'https://www.facebook.com/sharer/sharer.php?u=',
    TWITTER = 'https://twitter.com/intent/tweet?url='
}

export enum NotificationTemplate {
    PHONE_VALIDATION = 'Verificar.txt',
    WELCOME = 'Bienvenido.html',
    EMAIL_VALIDATION = 'Email_Validation.html',
    VERIFICATION_CODE = 'Verification_Code.html'
}

export enum EmailSubject {
    WELCOME = 'Bienvenido a paga.pe',
    EMAIL_VALIDATION = 'Validación de Email',
    VERIFICATION_CODE = 'Código de Verificación'
}

export enum ForgotPassword {
    SEND_EMAIL = '1',
    SEND_SMS = '2'
}