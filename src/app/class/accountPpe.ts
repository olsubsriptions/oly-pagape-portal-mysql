import { AccountParameter } from './accountParameter';

export class AccountPpe {
    organizationId: string;
    businessId: string;
    userName: string;
    documentType: string;
    documentNumber: string;
    businessName: string;
    businessAddress: string;
    userCode: string;
    link: string;
    country: string;
    parameters = new Array<AccountParameter>();
    // email: string;
    // movil: string;
    // solesBank: string;
    // solesAccountNumber: string;
    // solesCci: string;
    // dolarBank: string;
    // dolarAccountNumber: string;
    // dolarCci: string;
}