export class TtsRequest{
    TTSCode: number;
    TTSName: string;
    appID: string;
    Data: any;
    result: string;
}