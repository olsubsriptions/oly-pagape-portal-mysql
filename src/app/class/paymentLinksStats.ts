export class PaymentLinksStats{
    expired: string;
    paid: string;
    total: string;
    textTotal: string;
    pending: string;
}