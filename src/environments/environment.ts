// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api: {
        url: 'http://localhost:8100'
        // url: 'https://olsportalapi-dot-subcriptions-2020-02.wl.r.appspot.com'
    },
    console: {
        // url: 'https://olsconsoleapi-dot-subcriptions-2020-02.wl.r.appspot.com'
        url: 'http://localhost:8200'
    },
    tts: {
        // url: 'https://api.paga.pe:8081/ttsapi/operation'
        url: 'https://ttsapi.paga.pe/ttsapi/operation'
    },
    landingPage: {
        url: 'http://localhost'
        // url: 'https://www.paga.pe'
    },
    firebase: {
        apiKey: "AIzaSyAz15EjvUgJ-G1lQsPTYADCJeljYKvfLj0",
        authDomain: "subcriptions-2020-02.firebaseapp.com",
        databaseURL: "https://subcriptions-2020-02.firebaseio.com",
        projectId: "subcriptions-2020-02",
        storageBucket: "subcriptions-2020-02.appspot.com",
        messagingSenderId: "700465223773",
        appId: "1:700465223773:web:ac87110b1203e9acef95b4"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
