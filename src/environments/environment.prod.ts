export const environment = {
    production: true,
    api: {
        url: 'https://olsportalapi-dot-subcriptions-2020-02.wl.r.appspot.com'
        // url: 'http://172.16.3.140:8100'
    },
    console: {
        url: 'https://olsconsoleapi-dot-subcriptions-2020-02.wl.r.appspot.com'
        // url: 'http://172.16.3.140:8200'
    },
    tts: {
        url: 'https://ttsapi.paga.pe/ttsapi/operation'
    },
    landingPage: {
        url: 'https://www.paga.pe'
    },
    firebase: {
        apiKey: "AIzaSyAz15EjvUgJ-G1lQsPTYADCJeljYKvfLj0",
        authDomain: "subcriptions-2020-02.firebaseapp.com",
        databaseURL: "https://subcriptions-2020-02.firebaseio.com",
        projectId: "subcriptions-2020-02",
        storageBucket: "subcriptions-2020-02.appspot.com",
        messagingSenderId: "700465223773",
        appId: "1:700465223773:web:ac87110b1203e9acef95b4"
    }
};
