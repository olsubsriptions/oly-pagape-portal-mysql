# Firebase Deploy Angular (Angular v8.1)

## 1. Algunos detalles previos

### firebase.json
  - Cuando ejecutas `firebase init` este archivo se crea automáticamente. 
  - Archivo de configuración para agregar algunas opciones en el deploy (por ejemplo que archivos ignorar)
	- Mas información en el link [Firebase Full Config](https://firebase.google.com/docs/hosting/full-config#specify_files_to_deploy)
	
### .firebaserc
  Este archivo se genera cuando se está realizando el deploy de la app angular.

## 2. Deploying
Antes que nada nos ubicamos en la raiz del proyecto de angular ( cd myProjectAngular/ ). A continuación ejecutamos los siguientes comandos.
  
  1. `ng build --prod`

  2. `firebase login`
  
  3. `firebase init`

      Luego de ejecutar el comand se presentaran algunas preguntas en la ventana de comandos. Elegir lo siguiente:

      > ? What do you want to use as your public directory? (public) **dist/nombre-proyecto**
        
        Colocar el directorio dist/nombre-proyecto. Estos se generan cuando se ha ejecutado el paso 1.

      > ? Configure as a single-page app (rewrite all urls to /index.html)? **Yes**
        
        Escribimos o elegimos Yes.
        
      > ? File dist/ngx-os-portal/index.html already exists. Overwrite? **No**
        
        Elegimos que no sobreescriba nuestro archivo index.html	

  4. `firebase deploy`